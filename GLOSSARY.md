# SIG 
Système d'information (Géographique) conçu pour recueillir, stocker, traiter, analyser, gérer et présenter tous les types de données spatiales et géographiques. L’acronyme SIG est parfois utilisé pour définir les « sciences de l’information géographiques » ou « études sur l’information géospatiales ». Cela se réfère aux carrières ou aux métiers qui impliquent l'usage de systèmes d’information géographique et, dans une plus large mesure, qui concernent les disciplines de la géo-informatique (ou géomatique). Ce que l’on peut observer au-delà du simple concept de SIG a trait aux données de l’infrastructure spatiale.[source wikipédia](https://fr.wikipedia.org/wiki/Syst%C3%A8me_d%27information_g%C3%A9ographique) 

# SCR
Système de Coordonnée de Référence: modèle mathématique permettant, grâce aux coordonnées, de faire le lien entre un endroit réel sur terre et sa représentation en plan. Il faut bien faire attention au choix du système. Cela dépend de la taille de la zone de travail (commune, pays, continent), des analyses que l’on veut en tirer et, souvent aussi de la disponibilité des données.[source documentation QGIS](https://docs.qgis.org/2.8/fr/docs/gentle_gis_introduction/coordinate_reference_systems.html)
A l'Inrap en tant qu'institut national il est fortement recommandé d'utiliser les SCR2154 : RGF93 / Lambert-93

# Autre terme
Dans la définition on peut mettre du** gras** et *autre*, -etc-

