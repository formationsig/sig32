# Summary

## SIG perfectionnement 3.2: Analyses Spatiales


* [Préambule](/pas_a_pas_v2/3_2_1.md)


* [1 Articulation des données spatiales et descriptives](/pas_a_pas_v2/3_2_1.md#1-articulation-des-donnees-spatiales-et-descriptives)
	* [1.1 Les différents types de stockage de données](/pas_a_pas_v2/3_2_1.md#11-les-differents-types-de-stockage-des-donnees)
	* [1.2 Les données relationnelles](/pas_a_pas_v2/3_2_1.md#12-les-donnees-relationnelles)
	* [1.3 Les requêtes](/pas_a_pas_v2/3_2_1.md#13-les-requetes)


* [2 Traitement et représentation des données](/pas_a_pas_v2/3_2_2.md#2-traitement-et-representation-des-donnees)
	* [2.1 Requêtes, sélection et affichage : jointure à partir d'une relation de 1 à n](/pas_a_pas_v2/3_2_2.md#21-requetes-selection-et-affichage-jointure-a-partir-dune-relation-de-1-a-n)
	* [2.2 Représentations quantitatives](/pas_a_pas_v2/3_2_2.md#22-representations-quantitatives)
	* [2.3 Analyses et Représentations spécifiques](/pas_a_pas_v2/3_2_2.md#23-analyses-et-representation-specifiques)
		* [2.3.1 Représentation en oursin](/pas_a_pas_v2/3_2_.md#231-representation-en-oursin)
		* [2.3.2 Recollage de céramique](/pas_a_pas_v2/3_2_2.md#232-recollage-de-céramique)
		* [2.3.3 Calcul d'orientation](/pas_a_pas_v2/3_2_2.md#233-calcul-dorientation)
		* [2.3.4 Projection en élévation des données spatiales](/pas_a_pas_v2/3_2_2.md#234-projection-en-elevation-des-donnees-spatiales)

* [3 Exercices de réappropriation](/pas_a_pas_v2/3_2_2.md#3-exercices-de-reappropriation)

## Fiches mémo
* [memo SQL](/memoSQL/memoSQL.md)

## Glossaire

* [glossaire](GLOSSARY.md)
* [Catalogue des requêtes SQL utilisées dans la formation](/pas_a_pas_v2/3_2_3.md)