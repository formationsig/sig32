# ![SIG32](images/sig32.png) SIG 3.2: Analyses Spatiales



## Tables attributaires des données d'exercice



### F101055_Chilleurs_aux_bois

F101055_poly![img][001]

CADOC_Fait_SIG![img][002]

### F103066_Obernai

F103066_poly![img][003]

F103066_ouverture![img][004]

Obernai_ceram_LTD![img][005]

Obernai_lithique_LTD![img][006]

### F103455_Esvres

F103455_poly![img][007]

F103455_axe![img][008]

F103455_point![img][009]

S324_3202![img][010]

### F025228_Alizay

F025228_poly![img][011]

F025228_axe![img][012]

F025228_point![img][013]

[001]:images/tables/F101055_poly.png
[002]:images/tables/CADOC_Fait_SIG.png
[003]:images/tables/F103066_poly.png
[004]:images/tables/F103066_ouverture.png
[005]:images/tables/Obernai_ceram_LTD.png
[006]:images/tables/Obernai_lithique_LTD.png
[007]:images/tables/F103455_poly.png
[008]:images/tables/F103455_axe.png
[009]:images/tables/F103455_point.png
[010]:images/tables/S324_3202.png
[011]:images/tables/F025228_poly.png
[012]:images/tables/F025228_axe.png
[013]:images/tables/F025228_point.png

