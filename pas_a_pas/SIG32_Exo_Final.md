#  ![SIG32](images/sig32.png)SIG 3.2: Analyses Spatiales - Exercices Finaux

Ouvrir le projet **00_Alizay.qgz**

## Exercice 1: Collections de cartes de densités de mobilier

{% hint style = 'tip' %}

**Jeu de données** : (Alizay) F025228_point et F025228_Emprise

**Objectif**: Élaborer une collection de cartes montrant les densités (en NR/m²) de mobilier. 

* La  maille se composera de carrés de 10 m de côté.doit être découpée selon l’emprise.
* Il faudra effectuer 5 cartes: carte générale (densité de tous les mobiliers), carte de densité de la céramique, des charbons, du lithique et de la faune.

{% endhint  %}

----------------------------------------
## Correction

### A) Créer le carroyage

#### 1. préparer une grille de 10 m de côté :

Boîte à outils de traitements → ![qgis](images/icon_qgis.png)Création de vecteur → **Créer une grille**

**Paramètres:**

* *Type de grille* : **Rectangle (polygone)**
* *Étendue de la grille* : Utiliser l’emprise de la couche **F025228_emprise**
* *Espacement horizontal* : 10 m
* *Espacement vertical* : 10 m
* *Superposition horizontale et verticale* : 0
* *SCR* : **2154**
* *Grille* : Enregistrer vers un fichier → **F025228_grille10x10** 

#### 2. découper la grille selon l'emprise

Boîte à outils de traitements → ![qgis](images/icon_qgis.png)Recouvrement de vecteur → **Couper**

**Paramètres:**

* *Couche Source* : (que l'on veut découper) **F025228_maille10x10**
* *Couche de superposition*: (celle qui découpe) **F025228_emprise**

#### 3. Calcul de la surface de chaque maille

En effet certaines mailles découpée par l'emprise ne font plus 100m², il faut donc créer un champ surface pour pouvoir calculer la densité en NR/m².

Avec la **calculatrice de champs **![calc](images/icon_calc.png) , créer un nouveau champ "surface" avec la fonction `$area`



### B) Comptage des points par maille et calculs de densités

####1. Comptage des points par maille

Boîte à outils de traitements → ![qgis](images/icon_qgis.png)Analyse vectorielle → **Compter les points dans les polygones**

**Paramètres:**

* *Polygones* :  **F025228_grille10x10** 
* *Points* : **F025228_point**
* *Nom du champ de dénombrement* : laisser **NUMPOINTS**
* *Compte* : Enregistrer vers un fichier → **F025228_maille10x10**

#### 2. calcul de densité (tous les mobiliers)

Avec la **calculatrice de champs **![calc](images/icon_calc.png) , créer un nouveau champ :

* *Nom*: "densite"
* *Type*: **real**
* *Longueur*: **8**
* *Précision*: **2**
* *Expression*: `"NUMPOINTS"/"surface"`

#### 3. calcul de densité de chaque type de mobiliers

*Exemple pour le lithique:*

* A partir de la couche de points **F025228_point**, et avec  l'outil de **Sélection des entités selon une expression** ![selc](images/icon_select_epsilon.png), sélectionner uniquement le mobilier lithique grâce à la requête `"interpret" LIKE 'Lithique'`

* Boîte à outils de traitements → ![qgis](images/icon_qgis.png)Analyse vectorielle → **Compter les points dans les polygones**

  **Paramètres:**

  * *Polygones* :  **F025228_grille10x10** 
  * *Points* : **F025228_point**
  * ![attention](images/attention.png)**COCHER** *entité(s) sélectionée(s) uniquement*
  * ![attention](images/attention.png)*Nom du champ de dénombrement* : laisser **NP_lith**
  * *![attention](images/attention.png)Compte* : Enregistrer vers un fichier → **F025228_maille10x10_lith**



### C) SQL powah !

```sql
SELECT COUNT(p."numpoint") AS count_ceram,
	ST_Expand(ST_SnapToGrid(p."geometry", 10),5) AS geom
FROM F025228_point AS p
WHERE "interpret" LIKE 'Céramique'
GROUP BY geom
```

![attention](images/attention_color.png)Manque le découpage de la maille selon l'emprise !



### D) Symbologie

Si vous voulez **comparer les cartes de densités** en faisant une mise en page ou elles sont en regard les unes des autres vous devez **faire très attention à la méthode discrétisation choisie !**. 

Seules les méthodes suivantes permettent de comparer des cartes car elles ne sont pas dépendantes de la distribution des données ni de leur valeurs:

* Nombre égal (Quantiles)
* Écart-type

En outre il faut choisir  un vrai **dégradé de valeur** (niveau de gris ou camaïeu d'une seule couleur)


-----------------------------------------



### Exercice 2: Atlas des mobiliers projetés sur des axes

{% hint style = 'tip' %}

**Jeu de données** : (Alizay) F025228_point et F025228_axe

**Objectif**: Préparer un atlas montrant la projection des points de mobilier sur les axes correspondant au locus auxquels appartiennent les objets.

{%  endhint %}

-----------------------

### Correction

**↓↓↓ :construction:  A reprendre :warning: ↓↓↓**

### Reprojection des points

1. Reprojeter tous les points en une seule fois

```sql
SELECT p.numpoint, p.interpret, p.locus, p.US, p.Z, make_point(st_Line_Locate_Point(a.geometry,p.geometry)*st_length(a.geometry),p.Z*10) AS geomFROM F025228_axe AS a, F025228_point AS pWHERE p.locus LIKE a.numaxe
```

même requête que plus haut.

**`p.Z*10`** permet de multiplier par 10 l’échelle sur l’axe vertical pour y voir plus clair

1. Pour pouvoir créer un atlas, il faut une couche de couverture : on va reprojeter les axes sur le plan vertical :

2. comme on ne dispose pas de l’information sur l’altitude des axes et qu’on ne va pas les afficher sur les documents finaux, on les projette à l’altitude moyenne du nuage de points = **5,75 m** (environ)

calcul de l’altitude moyenne du nuage de points :
```sql
SELECT AVG(Z) 
FROM F025228_point
```

1. Reprojection des axes :

```sql
SELECT makeline(make_point(0,57.5),make_point(length(a.geometry),57.5)) 	AS geometry, a.numaxe 
FROM F025228_axe AS a
```
:warning: 57.5 et non 5.75 car l’échelle verticale est multipliée par 10


:warning: La requête globale fait planter l'ordinateur :

```sql
SELECT makeline(make_point(0,AVG(p.Z)*10),make_point(length(a.geometry), AVG(p.Z)*10)) 
	AS geometry, a.numaxe
FROM F025228_point AS p, F025228_axe AS a
```


1. Tous les points et les axes sont reprojetés au même endroit : la distinction entre les axes se fait avec un style de symbole, après avoir paramétré un Atlas

![img](https://lh4.googleusercontent.com/EIkU-Xf0efZ3Gf0Lyr9m2yPDZ2MobuqMQGztUaOfmIh1y87z-EBnc1p-IfxBUQypdigCohGivHA--xCpxIsWvFDVXv8mnGWpvbfbvreJlwoPT98IrgT9B0IzFkicVQsI6w9vl-lQ)



1. Atlas : dans le composeur d’impression

2. 1. Atlas avec comme couche de couverture **axe_reproj**
   2. Nom de la page : **numaxe**

![img](https://lh6.googleusercontent.com/7J82dg9zhSfUg0IiJb6_17-RJ0iKzWs1gDvZDFBSfLaYjnt2W_aQROW4e27LUL18uQNx-7cOCiY3zs9hg_yqwSu4Z7uuJ6DE-Lm830IVEflMoT-dCdczQoGeRNp7oyqgTohP4PY2)



1. La distinction entre les différents axes se fait avec un style de symbole : 

2. 1. style > ensemble de règles :

`locus LIKE @atlas_pagename`

1. 1. il faut ajouter cette clause à toutes les catégorisations éventuelles pour que ne s’affichent que les points correspondant à l’axe et au site dont il est question.



1. Comme les altitudes ont été multipliées par 10, il faut ajuster le graticule pour que les lignes horizontales d’altitude soient justes :

2. 1. nouvelle grille > modifier
   2. intervalle X : 0 ; **intervalle Y : 10**
   3. Afficher les coordonnées > Format > **Personnalisation** : `@grid_number /10`

# 