![SIG32](images/sig32.png) SIG 3.2: Analyses Spatiales

**Pré-requis**

- Avoir obligatoirement suivi la formation **SIG 1: Découverte** & **SIG 2: Figures**.
- Il est recommandé d'avoir suivi **Stat1 - Statistiques descriptives univariées** & **SIG 3.1 - BDD Relationelles et spatiales**
- Utiliser régulièrement et fréquemment QGIS
- Être capable de créer un jeu de données “propre”, tant sur le plan attributaire que géométrique.

**Objectifs principaux** 

- Articulation entre l’enregistrement descriptif et le SIG
- Concevoir/mettre en oeuvre le système d’enregistrement par rapport à la problématique
- Acquérir les bases de l’analyse spatiale.
- Représenter des données quantitatives et qualitatives.
- Maîtriser les bases du langage SQL.

**Objectifs secondaires**

- Acquérir des automatismes
- - Group Stat
  - Boîte à outils du menu traitement
- Savoir s’orienter dans le langage SQL
- - comprendre une requête
  - chercher de l’aide sur une fonction

**Jeux de données**

- **F103066_Obernai**: csv wkt ; nettoyage, consolidation des données ; cercles proportionnels ; diagrammes en oursin
- **F103455_Esvres** : Correction de géométrie / Script 6 couches / SQL requête / Calcul d’orientation / Ensemble de 
- **F101055_Chilleurs_aux_bois** : lecture
- **F108633_Saran** : sqlite / spatialite
- **F025228_Alizay** : analyse par maille


# Reprise en main de QGIS

Options du logiciel, encodage, paramétrage...

## 0.1. Rappel des fondamentaux des SIG (si nécessaire… )

paramétrage projet:

- paramétrages de QGIS pour les projets à venir
- paramétrage du projet en cours
- - vérifier SCR (2154)
  - chemins d’accès
  - demander le SCR pour les nouvelles couches

projection /reprojection → soit exporter, soit boîte à outils de traitement

## 0.2. Le dossier .qgis3

Le dossier **.qgis3** est créé par QGIS lors de son premier démarrage et comprend un certain nombre de dossiers « annexes » à l’installation, comme le dossier *project_templates* qui stocke les modèles que vous avez créés, le dossier *python* qui stocke les extensions chargées, etc…
Ce dossier peut être également alimenté avec vos propres fichiers ressources (symboles SVG, polices, …). 
Il est conseillé de mettre systématiquement les fichiers ressources (svg, modèles de mises en page, qml..) dans ce dossier.qgis3, afin de faciliter la sauvegarde de tous ces éléments, ainsi que leur utilisation dans des modèles.
Ce dossier est commun à toutes les versions successives de QGIS. Il ne doit donc pas être supprimé.

dans l’arborescence Windows
```
Windows 7 et 10 
C:\Users\compteUtilisateur\AppData\Roaming\QGIS\QGIS3\profiles
```
Par défaut, les extensions, les modèles, les modèles de composeur… sont situés dans ce répertoire. Il peut être copié d’un poste à l’autre pour conserver un certain nombre de réglages associés au logiciel.

Pour y accéder  depuis QGIS: Préférences > Profils utilisateurs > Ouvrir le dossier du profil actif

![profil_utilisateur](images/image002.png)

## 0.3. Projection / Reprojection

**Projection** : mise à plat d’une partie ou de la totalité de la surface courbe de la terre.

**Système de coordonnées de référence (SCR)** : modèle mathématique permettant, grâce aux coordonnées, de faire le lien entre un endroit réel sur terre et sa représentation sur plan. Le choix du SCR est important : il dépend de la taille de la zone de travail, des analyses que l’on veut en tirer… 

Dans le cadre de notre travail quotidien, il est fortement recommandé (obligatoire) de travailler en **RGF93/Lambert 93 (EPSG:2154) → système de projection légal en France.**

### 0.3.1 Projection à la volée

Pour activer/désactiver dans QGIS3 : Projet ⇒ Propriétés ⇒ SCR ⇒ cocher “aucune projection” pour désactiver la reprojection.

![projection_av](images/image003.png)

![attention](images/attention_color.png) La désactivation de la reprojection à la volée peut poser des problèmes dans le composeur d’impression (problème d’échelle, de graticules…). Il est donc fortement déconseillé de désactiver la reprojection à la volée.

### 0.3.2. Reprojection de couche

Rappel de SIG 1: Découverte:  

Il est possible que le fichier transmis par un collègue ne soit pas projeté dans le même SCR que le projet sur lequel vous travaillez (certains topos utilisent les SCR en conique conforme pour pallier les problématiques de déformation). Il convient dans ce cas d’effectuer une re-projection de couche (autrement dit, effectuer une transformation d’un SCR vers un autre SCR). Pour cela, le logiciel Qgis comprend des outils de re-projection.

> Menu Traitement ⇒ Boîte à outils ⇒ outils généraux pour les vecteurs ⇒ reprojeter une couche. 



# 1. Articulation des données spatiales et descriptives

**objectifs :** rappels des concepts fondamentaux de l’enregistrement stratigraphique communs à tous les systèmes d’enregistrement quels qu’ils soient; restituer les systèmes d’enregistrement de terrain à partir de la lecture des jeux de données; passage en revue circonstancié des différentes solutions existantes pour gérer les données spatiales et les données stratigraphiques.

## 1.1. Conceptualisation enregistrement / Comprendre un jeu de données

### 1.1.1. Rappels historiographiques et conceptuels

- L’enregistrement archéologique est une démarche scientifique dont l’objectif est de conserver la mémoire des vestiges fouillés (mobilier, immobilier, sédimentaires…).

- C'est une simplification du monde réel qui permet une restitution argumentée et interprétée de la stratification anthropique et naturelle fouillée, donc détruite.

- Les principes de l’enregistrement archéologique ont été édictées en Angleterre dans les années 60-70 et sont fondés sur le principe de superposition des strates géologiques mis en place au 17e siècle (et non remis en cause depuis).

- Les principes de l’enregistrement stratigraphique sont diffusés en France dès le début des années 70 et appliqués sur quelques sites emblématiques (Tours, Paris, Lattes...); matrice de Harris développée ensuite .

- La diffusion du « modèle harissien »  est incomplète dès le départ, en France.

### 1.1.2. Plusieurs approches (entrée par l’objet, l’US, la structure)

**Les différentes approches méthodologiques (stratigraphiques et spatiales)** pratiquées par les archéologues en fonction des périodes, des problématiques, des habitudes, ne sont pas contradictoires, mais elles s’insèrent dans un schéma logique de données FAIT ↔ US ↔ Artefact.



De manière résumée et peut-être un peu réductrice, 3 grandes approches cohabitent depuis une bonne trentaine d’années (d’après [Desachy 2008](https://tel.archives-ouvertes.fr/tel-00406241v2)) :

- l’analyse spatiale large : fouille en grand décapage dans la lignée des travaux des protohistoriens
- l’analyse spatiale fine type « ethnographique » (en référence à Leroi-Gourhan)
- un niveau médian : l’analyse par US pour les stratifications d’origine naturelle et/ou anthropique denses

*Avant les SIG*, l’accès à la vision en plan passait uniquement par le plan dessiné/illustré (plan masse, minute) : le plan illustrait le discours et les descriptions. Les spécificités morphologiques et topographiques étaient prises en compte mais pas forcément de manière systématique, dynamique et structurée.

*Avec la généralisation des SIG*, on passe de l’illustration au traitement de données : **la forme que l’on dessine dans l’espace devient elle-même une “donnée” qui n’est plus uniquement définie par sa description mais se définit aussi par des coordonnées et une position relative par rapport aux autres structures**.

D’un point de vue très concret, l’introduction des SIG a permis de prendre en compte et d’exploiter l’espace avant même sa description. Si cette manière de faire convient bien aux pratiques déjà en vigueur dans le cadre d’une “analyse spatiale large” et dans une moindre mesure (?) à l’analyse spatiale fine dite “ethnographique” où l’espace est une entrée privilégiée, elle “convient” moins aux opérations en milieu densément stratifié où la recherche/description/compréhension de la structure précède généralement sa représentation spatiale. C’est là que se pose plus spécifiquement la question de l’articulation entre l’enregistrement stratigraphique descriptif et l’enregistrement spatial (problème des unités qui n’ont pas de représentation spatiale).

Et quand les trois cas de figure se présentent sur un même chantier, il faut "jongler". Charge à l’archéologue de s’adapter et d’adapter son enregistrement spatial et descriptif à la stratification et la densité des structures.

### 1.1.3. Le coeur de tout système d'enregistrement archéologique:  la trilogie *Fait, US, artefact*

**le  coeur de tout système d’enregistrement archéologique** est la trilogie FAIT/US/artefact. 

> Restituer un MCD/MLD sommaire ensuite (1. le coeur de tout système, 2. le MCD simplifié d’une opération, 3. le MPD définissant les tables, les champs - dont clés primaires - et les relations).

Dans tous les systèmes, le coeur est le même : artefact, us, fait. Ensuite, on ajoute les unités techniques d’enregistrement voire des unités de regroupement archéologique. 

**Etape 1 : le coeur de tout système d’enregistrement**

![MCD_simple](images/image004.png)

On peut associer US et Fait, US-artefact mais on ne peut pas avoir un enregistrement avec des Faits, des artefacts, sans US.

> **Notes:**
>
> **MCD** : un **modèle conceptuel de données** a pour but de représenter graphiquement et de la façon la plus simple et la plus directe possible l’ensemble des données qui seront analysées tout en s’affranchissant de toutes les contraintes logicielles ou liées à la base de données sur laquelle va reposer l’application. Dans le MCD, on décrit les relations avec des mots. Formalisme entité-relation.
>
> - permet de modéliser la sémantique des informations d'une façon compréhensible par l'utilisateur de la future bdd
> - utilise le formalisme  *entité-relation*
> - ne permet pas l'implémentation informatique de la bdd dans un SGBD
> **MLD** : un **modèle Logique de données**
> - permet de modéliser la structure selon laquelle les données seront stockées dans le future bdd.
> - est adaptée à une famille de SGBD (ici relationnel).
> - utilise le formalisme Merise.
> - permet d'implémenter la bdd dans un SGBD.
> **MPD** : un **modèle physique de données** est un modèle directement exploitable par la base de données utilisée. C’est le bazar réalisé. Le passage du MCD au MPD peut se matérialiser par la création de tables intermédiaires dans le cas des relations de n à n (cf. ci-dessous).

**Etape 2 : MCD sommaire**

On ajoute les unités techniques; puis ajouter les relations pour établir le MCD

![MCD_sommaire](images/image005.png)

**Etape 2bis: intégrer l'espace**

Ajouter la géométrie des unités qui vont être représentées dans le système d'enregistrement: *points, lignes, polygones*

**Etape 3: MLD/MPD**

Définition des tables, des occurences et des liens de cardinalités.

![MLD_MPD](images/image006.png)

> **Rappel sur les relations** : 
>
> - **relation de 1 à 1** : signifie que pour chaque enregistrement d’une table il ne peut y avoir que 0 ou 1 enregistrement d’une autre table qui lui soit lié (ex : entre la table des faits et une table mobilier…). Dans une base de données classique, ce type de relation peut être évitée en la remplaçant par une la fusion (jointure ?) des deux tables en relation. 
> - **relation de 1 à** **n** (de un à plusieurs) : signifie que pour chaque enregistrement d’une table, il peut y avoir un ou plusieurs enregistrements d’une autre table qui lui soit lié (ex : entre la table des faits et la table des US).
> - **relation de** **n** **à** **n** (de plusieurs à plusieurs) : une relation existe quand un ou plusieurs enregistrements d’une table peuvent avoir une relation avec un ou plusieurs enregistrements d’une autre table (ex : entre la table des faits et la table des photos, où un fait peut être visible sur plusieurs clichés et où l’on peut voir plusieurs faits sur une photo). Dans le cas où l’on souhaite établir une relation de plusieurs à plusieurs entre deux tables, une troisième table est nécessaire pour stocker les combinaisons créées par la relation. Ce type de relation (*n* à *n*) revient à établir deux relations de un à plusieurs (1 à *n*). 

## 1.2. Les outils existants pour exploiter les données spatiales et descriptives

Différentes solutions techniques existent pour interroger conjointement données spatiales et données descriptives. 

On peut simplifier en évoquant 2 cas de figures distincts :
- soit il s’agit d’une combinaison entre QGIS et quelque chose et entre les deux, il y a de la "couture" à faire ;)

- Soit, tout est géré au même endroit.
  Évidemment chaque solution a ses inconvénients et ses avantages.  Du plus abordable (jointures, relations, spatiaLite) a plus complexe (PostGIS, l'exemple de Caviar).

  --------------------------------------

**![important](images/icon_important.png) Pendant la formation nous allons tester différentes solution. Le manipulations ci-dessous seront de 3 sortes:**

* **Démonstration**: seul le formateur fait les manipulations.

* **Exercice dirigé**: le formateur fait la manipulation puis les participants le font sur leur poste dans la foulée.

* **Exercice en autonomie:** Les participants (re)font les manipulations, puis le formateur fait une correction commentée.

---------------------------------------------------

### 1.2.1. Les différents types de stockage des données : *.shp, .csv/wkt*

#### 1.2.1.1. Le format *.shp*
bref rappel de SIG 1: Découverte qu'est ce que le format *shapefile*  (4, 5,ou 6 fichiers pour un shapefile), mais on sait faire !

#### 1.2.1.2. Le format *.csv* avec stockage des géométries en *WKT*
> Rappel: Les fichiers CSV sont des formats d’échange commun entre les logiciels gérant les données tabulaires et sont également facilement produits manuellement avec un éditeur de texte ou avec un programme ou des scripts écrits par l’utilisateur.

Le stockage des géométries en WKT (*Well Known Text*) s’opère dans un fichier en texte délimité, dans un champ spécifique où les géométries sont décrites de la manière suivante :  **TYPE DE GEOMÉTRIE (coordX CoordY des noeuds, X Y, X Y,...)** par exemple `POLYGON((1 1,5 1,5 5,1 5,1 1))`


> **Enregistrer en WKT:**  *(Exercice DIrigé)*
>
> **Jeu de données**: (Obernai) F103066_poly.shp
>
> ⇒ Ouvrir le fichier **Obernai F103066\F103066_vecteur_operation\F103066_poly.shp**
>
> ⇒ Clic droit / Exporter > Sauvegarder les entités sous...
>
> ![save_csv](images/image007.png)
>
> 1. Format “Valeurs séparées par une virgule [CSV]”
>
> 2. Renseigner le nom et le chemin d’accès au fichier **F103066_poly.csv**
>
> 3. Vérifier que le SCR correspond bien au Lambert 93 (EPSG 2154)...
>
> 4. … l’encodage à l’UTF-8
>
> 5. Dans les options de la couche, il faut choisir d’exporter la géométrie comme WKT
>
> 6. On peut ensuite choisir de créer un fichier .csvt (le fichier csvt associé au csv permet de conserver le formatage des champs lors de l’importation du csv créé)[^1]
>
>  7. On peut choisir de ne pas exporter tous les champs
[^1]: Dans QGIS, le module OGR gère la lecture et l’écriture de données essentiellement tabulaire non-spatiale dans des fichiers CSV texte. Lors de la lecture d’un champ nommé “WKT” (qui est supposé contenir une géométrie WKT), les informations stockées sont également traitées comme un champ normal. Le pilote CSV renverra tous les attributs des colonnes avec un type ‘‘chaîne de caractère’’ si aucun fichier d’information sur le type des champs (avec l’extension .csvt) n’est disponible.

Inversement, vous pouvez importer un fichier d’archive en **format WKT** dans un SIG :

> **Importer en WKT:** *(Exercice dirigé)*
>
> **Jeu de données**: Obernai (vecteur_operation_wkt/F103066_emprise.csv)
>
> ⇒ Aller chercher le fichier **F103066_emprise.csv** via l'icône **![icon_csv](images/icon_csv.png)**
>
> ![import_csv](images/image008.png)

Résultat: ( :construction: reprendre avec emprise)
![import_csv2](images/image009.png)

Note: Ici, en ouvrant la table attributaire, vous observez que les champs ont bien leur format d’origine (puisqu’on avait créé un fichier .csvt lors de la création du CSV) : les valeurs d’un champ ‘‘texte’’ sont alignées à gauche ; les valeurs d’un champ ‘‘chiffre’’ sont alignées à droite. 

> Rappel sur le vocabulaire : chiffres entiers (*Integer*), chiffres réels (*Real*), chaînes de caractères (*String*), dates (*Date : YYYY-MM-DD*)

### 1.2.2. Jointures

**RAPPEL du niveau 1**
Qu'est ce qu'une jointure ?
Relier deux tables de données. Relation de 1 à 1 uniquement (à 1 objet dans une table correspond 1 objet dans l’autre table). Nécessite un champ commun (clé primaire).

**Avantages :**
* Très facile à mettre en en œuvre dès lors que **les tableaux sont bien structurés** (que ces tableaux soient une extraction depuis une base de données ou saisis directement dans un tableur)
* Le format  **.odt** (format natif du tableur *LibreOffice Calc*) permet un dynamisme dans les 2 sens (on peut le modifier directement dans QGIS). ![attention](images/attention_color.png) le tableau ne doit pas être ouvert en même temps avec *LibreOffice Calc*.
* Une modification dans *MS Excel* d'une feuille **.xls** sera répercutée dans QGIS après réouverture du projet.  Le format **.xslx** permet la même "dynamisme" que pour le format **.odt**.

**Inconvénients :**

* Selon le format du fichier il n'y a pas de lien dynamiques donc pas de mise à jour automatique entre les tables de données dans le SIG la plupart du temps.
* Contraint par la relation de 1 à 1

>**Jointure** *(Exercice dirigé pour rappel...)*
>**Jeux de données**: (Chilleurs-au-Bois) F101055_Vecteurs/**F101055_poly.shp** & F101055_Tables/**CADOC_fait_SIG.xls**
>
>**Pas à Pas**
>
>Étape 1:
>
>* Joindre la couche **F101055_poly** avec le fichier **CADOC_fait_SIG.xls**
>
>* Enregistrer le projet
>
>* Modifier l’enregistrement du Fait 1000 dans *MS Excel*
>
>* Rouvrir le projet et la jointure
>
>* Cela fonctionne dans un seul sens xls → qgis et nécessite de fermer le tableau pour voir les modifications
>
>Étape 2:
>
>* Dans *LibreOffice Calc* Convertir le xls en .ods et fermer *LibreOffice*
>
>* Importer l'ods dans QGIS et  refaire la jointure avec l’ods
>
>* Dans *Calc* modifier la table (changer des valeurs), enregistrer, fermer le tableur.
>
> ⇒ Dans QGIS, constater que la modification a été prise en compte
> 
>* Dans QGIS modifier des valeurs dans la table
>
> ⇒  Dans *Calc*, constater que la modification a été prise en compte
> 
> Note: Il faut pour valider les modifications faites dans un logiciel fermer/réouvrir le projet (le tableau dans *Calc* / le *projet * dans QGIS).

**Précisions:**

* Une jointure est modifiable depuis la couche mère si les deux couches sont en mode édition.
* Lors du paramétrage de la jointure l'option *mettre la jointure en mémoire virtuelle* : permet de garder la jointure active dès l’ouverture du projet.

### 1.2.3. Les relations dans QGIS

![attention](images/attention_color.png) ceci est un rappel de la formation [SIG 3.1 BDD relationelle et spatiale](https://formationsig.gitlab.io/sig31/)

La mise en place de relations 1 à n (1 à plusieurs) entre couches/tables est possible dans QGIS et doivent être paramétrées au niveau du projet.

>Notes: Il est question de relation **attributaire** entre une *Table Mère* et une *Table Fille*. Cette relation grâce à **une clé primaire  ![pk](images/icon_pk.png) dans la *Table Mère*** que l'on peut retrouver en tant que **clé étrangère ![fk](images/icon_fk.png) dans la *Table Fille***. On ne parlera ici plus que de **table** que ce soit pour désigner un tableau ou la table attributaire d'une couche.

**Avantages :** rend la donnée plus accessible d'une table depuis une autre table en relation (Table Mère ↔ Table Fille)

**Inconvénient :** 

* La relation est contenue le projet QGIS uniquement.
* Elle ne permet que de consulter la donnée.

>**Relation de 1 à n** *(Exercice dirigé)* 
>**Jeu de données :** (Saran) F108633_fait.shp & Inv_US.xls
>
>**Étape 1: Paramétrage de la relation dans le projet**
>
>**Préalable:**
>Ajouter la couche **F108633_fait** (shapefile) et le tableau **Inv_US.xls**. ![attention](images/attention_color.png) penser à le passer en UTF-8
>
>**Objectif :** 
>Il s’agit ici de mettre en relation les deux couches, en considérant les caractéristiques suivantes : 
>**Table mère = F108633_fait** → clé primaire ![pk](images/icon_pk.png) **"num_fait"**
>**Table Fille = Inv_US** →  clé primaire ![pk](images/icon_pk.png) **"num_us" **et clé étrangère ![fk](images/icon_fk.png) **"num_fait"**
>⇒ Le champ à mettre en relation est donc **"num_fait" présent dans les 2 tables**.
>
>**Pas à Pas:**
>Menu Projet ⇒ Propriétés du projet
>![proprietes_projet](images/image010.png)
>
>1. Onglet [Relations]
>2. Cliquer sur [![+](images/icon_plus.png)Ajouter une relation]
>![fenêtre ajouter une relation](images/image011.png)
>1. **Nom:** Donner un nom court et explicite à la relation, ici **fait_us**
>2. **Id: ** recopier le même nom
>3. **Couche référencée (parentale):** Choisir la table mère, ici **F108633_fait**
>4. **Champ référencé:** Choisir le champ correspondant à la clé primaire ![pk](images/icon_pk.png) de la table mère
>5. **Couche de référence (Enfant):** Choisir la table fille, ici **F108633_fait**
>6. **Champ de référencement:** Choisir le champ correspondant à la clé étrangère ![fk](images/icon_fk.png) de la table fille qui permet de se "rattacher" à la table mère, ici **"num_fait"**
>7. Enfin laisser la **Force de la relation** sur *Association*

![attention](images/attention_color.png) Les relations une fois créées ne sont plus éditables: pour les modifier, il faut les supprimer et les refaire.

> **Étape 2: Paramétrage de la relation dans les formulaires d'attributs**
>
> **Objectif:** Pour observer la relation dans la consultation (via l'outil information ou directement via la table attributaire) sous la forme de formulaire:
>
> **Pas à Pas:**
>
>  * Menu Projet ⇒ Propriétés du projet
> * Onglet [![icon_form](images/icon_form.png)Formulaire d'attributs]
>  ![formulaire par glisser déposé](images/image012.png)
>  1. Choisir la **Conception par glisser/déposer**[^2]
>
>  2. Ne garder que les champs "num_fait", "typoly" et "ident" ainsi que la relation "fait_us"
>
>  3. Pour ajouter les champs de la couche US : cliquer sur ![+](images/icon_plus.png)...
>
>  4. ... et renseigner dans la fenêtre qui s'est ouverte: créer une catégorie = **US** en tant que **onglet**
>
>     puis glisser la relation (fait→us) dans le groupe créé.
>
>     ![formulaire_par_glisser_déposer_suite](images/image013.png)
>     Valider avec [OK]


[^2]: Il existe 3 modes de création de formulaire :  `Génération automatique`: garde la structure basique "une ligne - un champ" pour le formulaire mais permet de personnaliser chaque *widget* correspondant (per ex. faire une boite à cocher pour les champ en *1/0*, ouvrir un calendrier pour les champs *date*, etc) `Conception par glisser/déposer`: outre la personnalisation des *widgets*, la structure du formulaire peut-être complexifiée avec des regroupements de ceux-ci en groupes ou en onglets par ex.
`A partir du fichier .ui fourni`: Permet l'utilisation d'un fichier .ui créé avec le logiciel *Qt designer* et qui permet par conséquent une personnalisation plus avancée *mais avec des limitations qui peuvent être rédibitoires comme l'impossibilité de passer simplement d'un enregistrement à l'autre[ndlr]

⇒ Dès lors , si on identifie un fait on observe les US en relation avec le fait.

> **Étape 3: Visualiser la relation dans un formulaire d'attribut**
>
> **Objectif:** Accéder au formulaire nouvellement créé
>
> **Pas à Pas:**
> ![formulaire](images/image014.png)
>
> * Ouvrir la table attributaire de la couche **F108633_fait**.
> * Passer en mode  *Formulaire* grâce au bouton dédié ![icon_form](images/icon_form.png)en bas à droite de la fenêtre.
> * Dans le menu déroulant Expression  ⇒ Prévisualisation de colonne ⇒ "numfait"

On peut déjà utiliser cette petite mise en page de formulaire pour exploiter la relation et faire une recherche. En revanche on ne peut pas faire de requête exploitant cette relation. On verra plus tard [les requêtes SQL exploitant des relations](#212-jointure-de-1-à-n).

Pour la consultation, il est possible de:

* passer par la **barre d'outils des attributs** ⇒ Sélectionner des Entités par Valeur

  ![selection_valeur](images/image015.png)

* depuis le formulaire choisir l'icône Filtre

  ![formulaire_icone_filtre](images/image016.png)

### 1.2.4. Spatialite

L'utilisation d'une **base de données (BDD) relationnelle intégrée et interfacée dans QGIS** est possible ! Elle utilise le moteur de base de donnée relationnel **SQLite**.

![important](images/icon_important.png) Cette solution fait l'objet d'une formation dédiée: [**SIG 3.1: Base de Données relationnelle attributaire et spatiale** ](https://formationsig.gitlab.io/sig31/) nous ne faisons donc ici qu'une démonstration du fonctionnement d’une BDD relationnelle SQLite/spatialite dans QGIS.

**Principe :** 

![sqlite](images/logoSQLite.png) **SQLite** en quelques points:
* C'est un moteur de BDD relationnelle OpenSource, accessible par le **langage SQL**. 
* **Une base SQLite = 1 fichier** autonome, très léger et indépendant du système d'exploitation (contrairement à *MS Access* ou *FileMakerPro*) . 
* la BDD contient les tables, les relations, les requêtes SQL,des index,des  fonctions…

![sqlite](images/logoSpatialite.png) **Spatialite** est une extension de SQLite qui:

* Permet de gérer les géométries spatiales, toujours dans un seul fichier**.sqlite**

* L’extension Spatialite de SQLite est intégrée par défaut dans QGIS.

  

**Avantages :**

- Gestion et exploitation des données dans un seul et même outil

- Interopérabilité avec le reste du monde

- Facile à utiliser, *permet de s'initier réellement au langage SQL [ndlr]*

- Mono-utilisateur, il n'y a pas d'administration de BDD à faire

- Les styles peuvent être enregistrés dans la base 

  >  Note: il faut passer par le gestionnaire de connexion (Data source manager) ou par le **DB Manager** pour qu’ils soient appliqués. PAS CLAIR ??
  
  ![gestionnaire_de_BDD](images/image017.png)



**Inconvénients:**

- Ne gère pas les rasters

- Mono-utilisateur

**Démonstration:**

>**Jeu de données :**  Fouille de Saran, “La Motte Pétrée”, RO : Laurent Fournier:
>* 1 projet QGIS: **F108633_Saran_Motte_Petree.qgz**
>*  et  un fichier: **F108633_Saran_Motte_Petree.sqlite**
>
>On commence donc par exploiter un fichier/base de données préexistant.
>Ouvrir le projet QGIS **F108633_Saran_Motte_Petree.qgz**



Tout est déjà fait ! (et tout tient dans 2 fichiers):

* Le fichier QGIS gère uniquement l’affichage de la relation inter-table.

* Le reste est défini et administré par la base de données SQLite.

* Cela se gère comme on a l’habitude : un projet avec des couches.




### 1.2.5. PostGis (CAVIAR)

![PGis](images/logoPostgis.png) [**PostGis**](http://www.postgis.fr/) est une extension du Système de Gestion de Base de Données Relationnelle (SGBDR) ![PG](images/logoPG.png) [**PostgreSQL**](https://www.postgresql.fr/). C'est la cartouche spatiale de Postgresql à l'instar de Spatialite pour SQLite.

On utilise déjà PostGis  à l'Inrap quand on utilise **CAVIAR**, Le **CA**talogue de **Vi**sualisation de l'information **AR**chéologique qui est hébergé sur un serveur PostgreSQL/PostGis.

**Avantages :**

- C'est un système très puissant conçu pour la gestion de volumes de données importants (d’où l' :elephant: )
- On peut tout faire avec l'association Postgre/PostGis: manipuler des données descriptives et attributaires, "mélanger" les géométries, faire des vues...
- C'est une solution serveur qui permet l’édition et la consultation simultanée par plusieurs utilisateurs et depuis différentes interfaces (clients).

**Inconvénients :**
- Difficile d’utilisation
- Nécessité d'être administrée depuis une interface spécifique



> **Consulter une BDD PostGis** *(Exercice  dirigé)*
>
> **Jeux de données** : Caviar
>
> 
>
> **Pas à Pas**: 
>
> - Ouvrir QGIS si ce n’est déjà fait *(mais si c’est pas fait, c’est inquiétant...)*
> 1. Créer une nouvelle connexion à une base de données PostGis en cliquant sur l'icône dédié ![icon_pg](images/icon_pg.png)qui ouvre le *Gestionnaire de sources de données* (= *Data Source Manager*)
>
> 2. Cliquer sur [Nouveau] afin de paramétrer une nouvelle connexion
>
> 3. ![connexion_pg](images/image028.png)
>
>   ![param_connexion_pg](images/image029.png)
>
> - Renseigner les les paramètres suivants :
>   * Nom : **CAVIAR**
>   * Hôte : **10.210.1.32**
>   * Port : **5432**
>   * Base de données : **activite**
>   * nom d’utilisateur : **agent**
>   * mot de passe : **agent**
>   
> - De retour dans le *gestionnaire de source de données* :
>   - Cliquer sur [connecter]
>   - dérouler la base **activite** (clic sur ►)
>   - Ajouter les couches désirées  en double-cliquant dessus ou  1 clic + [Ajouter]
>   
>
> ![attention](images/attention_color.png) Quelques subtilités:
> * Les couches sont disponibles pour "toute la France" ou par DIR.
> * Si la couche est précédée d'un icône ![attention](images/attention_color.png): dans la rubrique **id de l'entité**, il faut choisir l'identifiant unique de la table **gid** .
> * PostGis pouvant stocker des couches avec des géométries différentes il est possible que l'on vous demande de choisir quelle géométrie affiché (pour les couches **uniteobservation** par exemple).


### 1.2.6. Requête et SQL : Le SQL c’est quoi et ca sert à quoi ?

Tout ce qui concerne le SQL dans ce déroulé est compilé dans le document [memoSQL](/memoSQL/memoSQL.md) 

#### 1.2.6.1. Do You Speak SQL ?[^3]
[^3]: cette partie explicative du langage SQL est fortement inspirée du site (http://www.commentcamarche.net/contents/1062-le-langage-sql)

Le **SQL** (Structured Query Language, traduisez Langage de requêtes structuré) est un **langage de définition de données, un langage de manipulation de données et un langage de contrôle de données** pour les[ bases de données relationnelles](http://www.commentcamarche.net/contents/105-les-modeles-de-sgbd). 

- Le SQL est un langage de **définition de données**, c'est-à-dire qu'il **permet de créer des tables dans une base de données relationnelle, ainsi qu'en modifier ou en supprimer**.
- Le SQL est un **langage de manipulation de données**, cela signifie qu'il **permet de sélectionner, insérer, modifier ou supprimer des données** dans une table d'une base de données relationnelle. 

Il est possible avec SQL de définir des permissions au niveau des utilisateurs d'une base de données.

![important](images/icon_important.png) En fait, le langage SQL peut rendre tous les services que l’on peut demander à une BDD relationnelle: Créer la structure de la BDD (créer des tables, créer les champs, remplir les données mais aussi créer les liens entre les tables et **SURTOUT interroger la BDD (C’est d’ailleurs essentiellement pour cela que nous allons l’utiliser)**. C’est ce que vous savez déjà tous faire (plus ou moins) sans forcément vous en rendre compte.

Avant tout le SQL est donc un langage de requête structuré il respecte donc une **syntaxe générale** de type (pour sa partie liée à l’interrogation et à la manipulation de données existantes) :

**SELECT** **liste des champs** **FROM** **liste des tables** **WHERE** **conditions**

- La partie **SELECT** indique les champs qui doivent apparaître dans la réponse. 

> Note: ces champs sont soit des champs existants dans les tables déclarées derrière la clause FROM soit le résultat d’un calcul. Il faudra dans ce cas leur donner un nom.

- La partie **FROM** décrit les tables qui sont utilisées dans la requête.

- La partie **WHERE** exprime les conditions, elle est optionnelle.

![important](images/icon_important.png)Quand vous faites une sélection dans QGIS la fenêtre expression correspond aux conditions c’est à dire ce qui suit la clause **WHERE**. Il faut imaginer que le logiciel fait précéder votre expression de la phrase :

**SELECT** ***** **FROM** **couche en cours** **WHERE**

> Où **\*** signifie **tous les champs**



Quand on utilise QGIS régulièrement on manipule déjà le langage SQL ... 

**Les opérateurs de comparaisons**

La clause **WHERE** est définie par une condition qui s'exprime à l'aide d'opérateurs de

comparaison et d'opérateurs logiques.

| A = B                                                        | A égal B                                                     |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| **A <> B**                                                   | A différent de B                                             |
| **A < B**                                                    | A plus petit que (inférieur à) B                             |
| **A > B**                                                    | A plus grand que (inférieur à) B                             |
| **A <= B**                                                   | A inférieur ou égal à B                                      |
| **A >= B**                                                   | A supérieur ou égal à B                                      |
| **A BETWEEN B AND C**(ne fonctionne pas avec les Virtual Layers) | A est compris entre B et C                                   |
| **A IN (B1, B2,...)**                                        | A appartient à liste de valeur (B1,B2,..)                    |
| **A LIKE 'chaîne de caractères'**                            | permet d’insérer des caractères jokers:% désignant 0 à plusieurs caractères quelconques_ désignant un seul caractère |



**Les opérateurs logiques**

**OR :** pour séparer deux conditions dont au moins une doit être vérifiée.

**AND :** pour séparer deux conditions qui doivent être vérifiées simultanément.

**NOT :** permet d'inverser une condition.

**AND** est prioritaire sur **OR** (en cas de doute, ne pas hésiter à mettre des parenthèses pour prioriser les sous-requêtes.



L'ordre de priorité des opérateurs est indiqué dans le tableau suivant. Un opérateur de priorité élevée est évalué avant un opérateur de priorité basse.

| Niveau           | Opérateur                                                    |
| ---------------- | ------------------------------------------------------------ |
| 1 (niveau élevé) | ~ (NOT au niveau du bit)                                     |
| 2                | * (Multiplication), / (Division), % (Modulo)                 |
| 3                | + (Positif), - (Négatif), + (Addition), + (Concaténation), - (Soustraction), & (AND au niveau du bit), ^ (OR exclusif au niveau du bit), \| (OR au niveau du bit) |
| 4                | =, >, <, >=, <=, <>, !=, !>, !< (opérateurs de comparaison)  |
| 5                | NOT                                                          |
| 6                | AND                                                          |
| 7                | ALL, ANY, BETWEEN, IN, LIKE, OR, SOME                        |
| 8 (niveau bas)   | = (Affectation)                                              |



Lorsque deux opérateurs dans une expression ont le même niveau de priorité, ils sont évalués de gauche à droite en fonction de leur position dans l'expression. 

> **Requête SQL simple** *(Exercice dirigé .. Rappel)*
> **Jeu de données**: Saran_BDD.sqlite (![attention](images/attention_color.png) IDEM F108633_Saran_Motte_Petree.sqlite ??)
> 
>**Objectif:** Depuis le *Gestionnaire de BDD*, faire une requête SQL simple.
> Pour savoir comment accéder à la fenêtre SQL voir dans le [memoSQL](/memoSQL/memoSQL.md##le-gestionnaire-bd)
> 
> Sélectionner les trous de poteau et les fosses fouillés.

#### 1.2.6.2. Manipulation, interrogation de données

![important](images/icon_important.png)Avant de se lancer dans une requête SQL la première chose à faire est de **s’approprier la table attributaire** afin de repérer où se trouvent les informations que l’on recherche. Cette étape indispensable peut se faire de plusieurs façons mais elle intègre toujours un passage en revue de la table.

> Note: Pour les exercices propre à cette formation vous trouverez en fin de document un récapitulatif des tables. **FAIRE UN LIEN**


> **Consultation et interrogation simple des données attributaires** *(Exercice dirigé)*
> **Jeu de données**: F103455_Esvres
> **Présentation de la Fouille d'Esvres (37) **: le contexte [Fiche sur Inrap.fr](http://www.inrap.fr/nouvelles-decouvertes-sur-la-necropole-gauloise-et-gallo-romaine-d-esvres-sur-5308) et le jeu de données.
>
> Fouille d'une nécropole GR. Enregistrement à l'échelle de la sépulture.
> Unité d'enregistrement spatial = le point 
> Unité d'enregistrement descriptif = UE.Ordre qui permet d'enregistrer mobilier, éléments architecturaux et anthropologiques.
>
> F103455_emprise.shp
> Esvres_poly.shp (essentiellement des fosses de sépultures)
> F103455_axe.shp (Axes pour profil longitudinaux et transversaux)
> F103455_point.shp (contient les points: unité d'enregistrement spatial)
>
> **Pas à Pas**
>
> * Ouvrir la table attributaire de **F103455_point**
>
> * Passer en revue les champs et tenter de qualifier le type de variables (clef primaire ![pk](images/icon_pk.png), éventuelles clefs étrangère![fk](images/icon_fk.png), variables qualitatives -texte-, variable quantitative…),
>
>   ![ex_table_point](images/image033.png)





On observe que la table a été structurée pour décrire les objets de la tombe selon les champs **"Type"**, **"matiere"** et  **"type_obj"**. Il serait intéressant de lister toutes les occurrences (lister les valeurs uniques) pour chaque champs ou mieux les différentes occurrences d’association entre ces 3 champs :

{% hint style='tip' %}

**Lister les valeurs uniques d'un champ**

>**Jeu de données**: (Esvres) F103455_point 
>
>**Objectifs: **Lister les valeurs uniques d'un champs.

**pas à pas:**

Menu [Traitements] ⇒ Boîte à outils de traitements ⇒ ![qgis](images/icon_qgis.png)Analyse vectorielle ⇒ **Liste les valeurs uniques**

![ex_table_point](images/image034.png)

1. Couche à interroger
2. & 3. Choix des champs dont on veut interroger les valeurs

**Résultat :** 18 combinaisons différentes

![resultat_valeurs_uniques](images/image035.png)
{% endhint %}

> **Sélectionner ou filtrer des données attributaires** *(Exercice dirigé...Rappel)*
>
> **Jeu de données**: (Esvres) F103455_point
>
> **Objectif:** Identifier/Localiser les points correspondants a des **clous en cuivre** .
>
> 1. D’abord, Il faut avoir sélectionné la bonne couche dans le panneau couche !
> 2. **Pour sélectionner** (résultat sélectionné, peut être copié…, apparaît en jaune sur le canevas de la carte et en bleu dans la table) : ![SQL_icon_selection_expression](images/icon_select_epsilon.png) Sélectionner les entités en utilisant une expression
> 3. Pour filtrer (n’affiche que les entités résultant de la requête sur le canevas et la table) ⁠⁠⁠:
> [Ctrl]+[F]
>
> ![ex_table_point](images/image036.png)
>
> 4. Dans les deux cas, taper la requête :
> ```sql
> "Type" LIKE 'Clou' AND "Matiere" LIKE 'Cuivre'
> ```
> ![ex_table_point](images/image037.png)
> Note:  Si le panneau valeur (à droite) n’est pas affiché, mettre le curseur à droite du panneau fonction et glisser déposer vers la gauche.



![important](images/icon_important.png) **Rappels:**
- `"Champ”` **opérateur** `'valeur'`  `AND`/`OR` `"Champ”` **opérateur** `'valeur'`  

- Les **champs** sont à mettre entre guillemets (touche[3]) et les **valeurs textuelles** entre apostrophes (touche[4]).

- Ne pas confondre **FILTRE** et **SELECTION**

  

Il est conseillé d’utiliser un logiciel de type [notepad++](https://notepad-plus-plus.org/fr/) pour enregistrer les requête SQL que l’on souhaite garder en mémoire. Menu Langage > SQL.



> **Requête SQL simple depuis la table attributaire** *(Exercice en autonomie)*
>
> **Jeu de données**: (Esvres) F103455_point
>
> **Objectif: ** Identifier les points correspondants à du mobilier en verre
>
> ```sql
> Type" LIKE 'Mobilier' AND "Matiere" LIKE 'Verre'
> ```

![important](images/icon_important.png)**Rappel** `ILIKE` (*to move it move it*:musical_note:)

* Correspond à `LIKE` mais ne tient pas compte de la casse 
* ![attention](images/attention_color.png)n'est pas supporté par le couches virtuelles (*virtual layer*)



#### 1.2.6.3. Le gestionnaire BD

Menu [Base de données] ⇒ Gestionnaire de BD
![Menu_DBmanager](images/image025.png)

![DB Manager](images/image030.png)
1. Une fois dans le *Gestionnaire de BDD* pour écrire une requête SQL cliquer sur l'icône requête SQL ![icon_requete_SQL](images/icon_reqSQL.jpg)

{% hint style= 'info' %}

Le gestionnaire de BDD permet d'utiliser du "vrai" SQL et par conséquent il faut écrire toute la requête.



**Les clauses SQL :** (clause = Partie d'un ordre SQL précisant un fonctionnement particulier)

| **SELECT**    | Précise les colonnes qui vont apparaître dans la réponse     |
| ------------- | ------------------------------------------------------------ |
| **FROM**      | Précise la (ou les) table intervenant dans l’interrogation   |
| **WHERE**     | Précise les conditions à appliquer sur les enregistrementsOn peut utiliser des comparateurs : =, >, <, >=, <=, <>des opérateurs logiques : AND, OR, NOTles prédicats, IN, LIKE, NULL, ALL, SOME, ANY, EXISTS... |
| **GROUP BY**  | Précise la (ou les) colonne de regroupement                  |
| **HAVING**    | Précise la (ou les) condition associée à un regroupement     |
| **ORDER BY**  | Précise l’ordre dans lequel vont apparaître les lignes de la réponse :ASC : en ordre croissantDESC : en ordre décroissant |
| **LIMIT (n)** | Permet de limiter le calcul au n premiers enregistrements    |
| **DISTINCT**  | Permet d’éviter les redondances dans les résultats (il s’agit d’une option à la commande SELECT). |
| **COUNT()**   | Permet de compter le nombre d’enregistrements dans une table. |

{% endhint %}



2. Dans la fenêtre qui s'ouvre tapez la requête SQL:

```sql
SELECT * 
FROM tabFait 
WHERE ident IN ('Trou de poteau', 'Fosse') AND statut LIKE 'fouillé'
```
puis cliquer sur le bouton ![Exécuter F5](images/bouton_executer.jpg)

![DB Manager](images/image031.png)
Il est possible de stocker et d’exploiter cette requête de différentes manières

1. **Enregistrer la requête** (donner un nom court et expressif):
 ⇒ valable pour le projet en cours.

  * Taper le Nom de votre requête

  * Cliquer sur le Bouton [Enregistrer]

  * La requête est maintenant accessible dans le menu déroulant *Requête enregistrée*

2. **Charger en tant que nouvelle couche** : 
 ⇒ valable pour le projet en cours.

  * Choisir du champ contenant la clé primaire (valeur unique)

  * Choisir le  champ contenant les géométries (générale ment "geom"

  * Donner un nom à la nouvelle couche 

  * [Charger]

    ⇒ valable pour le projet en cours

3. **Créer une vue** : 
⇒ enregistrée “en dur” dans la base de données, indépendant du projet en cours

  * Cliquer sur [Créer une vue]

  * Lui donner un nom (explicite et court et qui commence par vue_ par ex.)

  ⇒ La vue résultant de la requête est enregistrée dans la base de données ⇒ elle apparaît dans la liste des éléments de la base de données à gauche

>  Si la vue ne s'affiche ne s'affiche pas rafraîchir la vue ![F5](images/icon_F5.png)

![DB Manager](images/image032.png)

![important](images/icon_important.png) Une **vue** dans une base de données est une synthèse d'une requête d'interrogation de la base:

* On peut la voir comme une table virtuelle, définie par une requête. 
* Les enregistrements ne sont pas modifiables directement, il faut modifier la table de départ.
* Avantage : système dynamique, toute modification de la table de départ (ajout, suppression ou modification d’un enregistrement) sera répercutée dans la vue.


### 1.2.7. Virtual Layer

Il est possible dans QGIS d’utiliser le SQL pour créer **des couches virtuelles** (*virtual layer*).

source : [documentation QGIS](https://docs.qgis.org/3.10/fr/docs/user_manual/managing_data_source/create_layers.html?highlight=couche%20virtuelle#creating-virtual-layers)

Une couche virtuelle:

*  C’est une couche vecteur résultant d’une requête SQL sur une ou plusieurs couches vectorielles
* Elles correspondent à des vues sur d’autres couches et ne stockent donc pas de données
* Elles sont dynamiques: si les couches d’origine sont modifiées ce sera pris en compte par la couche virtuelle résultante.

Les couches virtuelles permettent donc de faire des requêtes sur **n’importe quelle couche ou table présente dans le projet QGIS** pour créer une nouvelle couche. En outre, ces couches sont dynamiques dans le sens ou si les données des couches d’origine sont modifiées ce sera pris en compte par la couche virtuelle résultante (= des vues).

> **Créer une couche virtuelle *(Virtual Layer)*** *Exercice dirigé*
>
> **Jeu de données**: (Esvres) F103455_point
>
> > **Objectif:** Créer une couche virtuelle de points contenant tous les points correspondant à du **mobilier en verre** 
>
> ![Virtual_layer](images/image038.png)
> 1. **Donner un nom** à la couche que vous allez créer : **points_verre**
> 2. (facultatif) **Importez** les couches que vous allez requêter : F103455_point. Le fait d’intégrer les couches permet de ne pas avoir à les charger dans le projet pour les requêter.
> 3. Tapez une requête SQL 
> ```sql
> SELECT N_PT, Type, Matiere, geometry FROM F103455_point WHERE Type LIKE 'Mobilier' AND Matiere LIKE 'Verre'
> ```



**Explication de ~~texte~~ code:**

* **`SELECT`** suivi des champs que l’on veut voir apparaître dans la table attributaire résultante, ici **"N_PT"**,**"Type"**,**"Matiere"** et ne pas oublier **"geometry"**
  ⇒ Comme le résultat est une couche de points il faut ajouter le champ **"geometry"** (il existe dans toutes les couches vectorielles mais n'apparaît pas dans la table attributaire)

> Dans QGIS le champ contenant la géométrie s'appelle **"geometry"** en revanche dans les couches Spatialite ce champ s'appelle **"geom"**

- **`FROM`** suivi du nom de la couche sur laquelle la requête va porter : **F103455_point**

- **`WHERE`** suivi des conditions de la requête comme vous l'écririez dans le panneau expression de l’outil *selection by expression* : `"Type" LIKE 'Mobilier' AND "Matiere" LIKE 'Verre'`

![Attention](images/attention_color.png)  Quand on commence à taper les premières lettres d’un champ, le logiciel nous propose des noms de champs. On peut choisir le champ désiré et valider avec [Entrée], le nom du champ s’affiche alors en noir et sans guillemets, cela fonctionne généralement MAIS il est conseillé de mettre les noms de champs entre guillemets.

![Attention](images/attention_color.png) Il faut faire attention aux noms des champs qui ne doivent pas correspondre à des fonctions SQL, éviter “date”, “type” (entre autres)

![Attention](images/attention_color.png) On pourrait utiliser **\* **pour faire apparaître tous les champs mais il est recommandé de les lister.

![Attention](images/attention_color.png) Dans le langage SQL on peut “baliser” des commentaires avec les caractères `/*` (début) et `*/` (fin). Ceci peut être très utile pour expliciter les requêtes complexes.

>4. Laisser la case **Autodétecter** cochée ou éventuellement cocher le dernier bouton puis remplir: colonne géométrique = geometry, Type = Point et EPSG = 2154.
>5. Cliquer sur le bouton [**Tester**] pour tester la syntaxe de votre requête SQL

![Attention](images/attention_color.png) Seule la syntaxe est vérifiée il faudra vérifier le résultat !
>6. Cliquer sur [OK]
>7. **Vérifier le résultat:** 

⇒ la couche doit apparaître dans le panneau couche avec le bon nom (point_verre) et la géométrie associée (un point).
⇒ vérifier la table attributaire: les noms de champs et la présence d’enregistrements.

>8. Si ce n’est pas le cas, on peut revenir sur la requête d’une couche virtuelle par un clic droit ⇒ **Éditer les paramètres de la couche virtuelle** 

⇒ Décocher l’affichage de la couche **F103455_point**: On peut repérer graphiquement les sépultures (**couche F103455_poly**) qui contiennent des objets en verre.

*Note: nous verrons plus tard comment sélectionner les sépultures qui continent du verre en exploita tant la relation de 1 à n entre ses 2 couches. [2.1.2. jointure à partir d'une relation de 1 à n](#2-1-2-jointure-à-partir-dune-relation-de-1-a-n)

> Éditer une couche virtuelle contenant les points, leur numéros, les numéros de fait, et le type d’objet des entités en métal qui ne sont pas des clous :

```sql
SELECT n_pt, fait, type_obj, geometry 
FROM F103455_point 
WHERE Matiere LIKE 'Métal' AND Type <> 'Clou'
```

> Éditer une couche virtuelle contenant la liste des éléments de rivets dont l’altitude est supérieure ou égale à 84,20 m NGF et indiquant leur numéro de point, le fait auquel ils appartiennent, l’identifiant de l’objet auquel ils correspondent et leur altitude

```sql
SELECT n_pt, fait, ue, z 
FROM F103455_point 
WHERE Type_obj LIKE 'rivet' AND Z >= 84.20
```
⇒ observer, décrire (changement d'icône…)

 ![virtual_layer_icone](images/icon_virtual.png)


## 1.3. Préparation des données

La préparation, la vérification et le nettoyage des données est une étape indispensable avant tout traitement complexe. La plupart des outils nécessaire à cette étape peuvent être trouvé via la **boîte à outils de traitement**.

La boîte à outils propose des outils qui vont servir aussi bien à la préparation/vérification/nettoyage des jeux de données qu’à l’analyse des données.

Quelques outils à connaître:

- analyse vectorielle : liste des valeurs uniques, compter points dans polygones…
- base de données : exporter dans PostgreSQL/SpatiaLite
- création de vecteurs
- géométrie vectorielle
- outils généraux pour les vecteurs

### 1.3.1. Scripts 6 couches

Il existe des modèles (créés grâce au modeleur graphique ou *model builder* de QGIS) aidant à structurer des couches pour préparer leur intégration dans CAVIAR.

Modèles de structuration de shapefile afin de les structurer en s’inspirant du modèle CAVIAR. Rajoute les champs nécessaires avec les bons noms et types.

Pour rappel, il existe une fiche mémo des [6 couches](https://docs.google.com/a/inrap.fr/viewer?a=v&pid=sites&srcid=aW5yYXAuZnJ8cmVzZWF1LXJlZmVyZW50cy1zaWd8Z3g6MTY2NTliYzRjNzc1Yjk4Yg) en PDF.

> **Installer un modèle dans la boîte de traitement**
>
> **Jeu de données**: dossier contenant les modèles à décompresser = models.zip contenant 6 fichiers de type *6couches_axe.model3*
>
> **Objectif:** Installer les 6 fichiers/modèles
>
> 2 manières:
> ![Attention](images/attention_color.png) Note : ne pas copier  ***6couches_axe.model3***, il sera fait ensemble
>
> 1. Boîte à outils de traitements ⇒ ajouter un modèle à la boîte à outils ⇒ Choisir les fichiers de modèle un par un pour les installer.
>
>   >  Note: Les  modèles seront copiés automatiquement dans le dossier `C:\Users\[nom_utilisateur]\AppData\Roaming\QGIS\QGIS3\profiles\default\processing\models`
>
> 2. En copiant, à la main, les fichiers de modèle dans le dossier `C:\Users\[nom_utilisateur]\AppData\Roaming\QGIS\QGIS3\profiles\default\processing\models`
>     ⇒ dans ce cas, il faut relancer QGIS pour que l’installation soit effective
>
>   ![ajouter_modele](images/image039.png)

> 
>
> **Jeu de données:** Esvres_poly.shp (![attention](images/attention_color.png) Attention la couche en sortie F103455_poly.shp sera réutilisée par la suite)
>
> **Objectif:** appliquer le modèle des 6 couches pour obtenir la bonne structuration respectant le modèle des 6 couches à partir de la couche Esvres_poly et pour obtenir la  couche F103455_poly.shp en sortie.
>
> **Correction-Démarche:**
>
> * Depuis la boîte de traitement, appliquer  le script *6couches_poly.model3*
> * Avec la calculatrice de champs, remplir le champ "numpoly" avec les valeurs du champ "num_fait"
> * Supprimer le champ "num_fait"



**Explications sur le fonctionnement du modèle :**

Le **modeleur** est un outil qui permet de préparer et paramétrer une chaîne de traitement.

Pour observer un exemple en détail:

* Clic-droit sur le modèle ***6couches_poly***  *Éditer modèle*

* Dans la fenêtre du Modeleur:
	* A gauche, les options
	* A droite, les traitements du modèle
	* Dans l’onglet [Entrée], les éléments à paramétrer en entrée lorsque l'outil sera lancé.
	* Dans l’onglet [Algorithme], on trouve tous les outils disponibles au travers de la boîte à outils de traitements pour les utiliser dans le modèle.

En cliquant sur le crayon ![crayon](images/icon_crayon.png) à côté de chaque traitement on accède à ses paramètres :

- *poly_entree* : création d’un couche à partir d’un couche en entrée
- *Add numpoly* : ajout du champs numpoly de type Entier.
- etc. pour les autres champs
- *poly_sortie* : nom de la couche temporaire qui sera ajouté dans la liste des couches du projet.



> **Créer son propre script grâce au modeleur graphique** *(Exercice dirigé)*
>
> **Jeu de données**: (Esvres) F103455_
>
> **Objectif:** Création du modèle de structuration des axes : **6couches_axe.model3**
>
> 1. Boîte à outils > créer un nouveau modèle
>    ![modele_new](images/image040.png)
> 2. Renseigner le nom (**6_couches_axe**) et le groupe (**shp_operation**) auquel appartiendra le modèle.  ![modele_new](images/image041.png)
> 3. Onglet Entrées ⇒ Couche vecteur ⇒ définir les paramètres:
> * Nom du paramètre : **couche_axe** (définit le nom de la couche appelée)
> * Type de géométrie : **ligne** (impose le type de géométrie, ici des lignes)
>    ![modele_new](images/image042.png)
> 4. Onglet Algorithmes ⇒ Table vecteur ⇒ Ajouter un champ à la table des attributs
> * Nom du champ : **numaxe**
> * Type de champ : **chaîne de caractère**
> * Longueur du champ : **10**
>    ![modele_new](images/image043.png)
> 5. Onglet Algorithmes > Table vecteur > Ajouter un champ à la table des attributs
> * Couche source : 'Ajouté' from algorithm 'Ajouter un champ à la table des attributs'
> * Nom du champ : typaxe
> * Type de champ : chaîne de caractère
> * Longueur du champ : 10
> * Ajouté : numSGA_axe
>    ![modele_new](images/image044.png)
> 6. Résultats : pensez à enregistrer le modèle.
>    ⇒ Par défaut, il sera enregistré dans QGIS3\*nom_profil*\default\processing\models
>    ![modele_new](images/image045.png)

Vous avez fait votre 1er modèle de traitement ! reste plus qu'à le tester...

>**Tester le modèle** *(Exercice dirigé)*
>
>**Jeu de données**: (Esvres) F103455_axe
>
>**Objectif:** Tester votre modèle sur une couche  shape
>
>Ouvrir la table attributaire de **F103455_axe**
>
>**Remarque :** en théorie, on reçoit/récupère un ou plusieurs shapes fournis par le topo. C’est la donnée brute. Cette donnée ne doit pas être modifiée et doit être conservée en l’état. On ne modifie donc pas la structure des shapes bruts et on en crée de nouveaux qui serviront au traitement de la données.
>
>
>
>1. Boîte à outils ⇒ Modèles ⇒ shp_operation ⇒ poly
>	* *poly_entree* : **F103455_axe**
>	* laisser le reste en l’état
>	* **[Exécuter]**
>	![modele_new](images/image045.png))
>2. **Un nouveau shape (temporaire)** apparaît dans la liste des couches.
>	Ouvrir la table attributaire pour observer les champs et constater la création des nouveaux champs.
>	Dès lors, certains champs peuvent **être mis à jour**. Par exemple, numpoly.
>	Mise à jour soit par la **calculatrice de champ**, soit par la **barre de formule** une fois passé en Mode Édition.
>3. Ne pas oublier d’enregistrer en dur le nouveau shape mis en forme : **F103455_axe**

> *Note: Vous en voulez encore ? Il existe un tutoriel sur le model builder sur [sigterritoires](https://www.sigterritoires.fr/index.php/tutoriel-modeleur-graphique-de-qgis-2-8/)*



#### :skull: FIN DE LA PREMIÈRE JOURNÉE :skull:



### 1.3.2. Transformation / consolidation

>
**Exercice** : Homogénéisation des valeurs au moyen de la calculatrice de champ
**Objectif** : Uniformiser les données attributaires
**Jeu de données** : (Obernai) F103066_poly
> 
Ouvrir la table attributaire de **F103066_poly**, et observer.

Dans le champ "interpret", il y a des valeurs en “US - …” et en “NF - …” (pour “ non fouillé ”) : il faut homogénéiser les valeurs afin d’avoir des cartographies “automatiques” et des statistiques / requêtes justes.


Il y a ici des informations de nature différente : on va vouloir mettre l’information “US” dans le champ “typoly” ; et l’information “NF” dans un nouveau champ “EtatFouille”. Puis, homogénéiser les valeurs des types d’entités (par exemple, mettre “fosse” pour “US - Fosse”, “NF - Fosse” et “fosse” ; etc.).

**Lister des valeurs uniques** *(Exercice en autonomie)*
**Objectif**  Chercher les valeurs uniques du champ **"interpret"**

Boîte à outils de Traitements ⇒ Analyse vectorielle ⇒ Liste des valeurs uniques
Note: Déjà fait sur les données de Esvres.

> **Compléter le champ "typoly" afin d'identifier les 'US'** *(Exercice en autonomie)*
> **Corrigé pas à pas: **
> Dans le champ “interpret”:
>* Sélectionner les valeurs en 'US - …'' (à la main ou via ![selection_expression](images/icon_select_epsilon.png) `"interpret" LIKE '%US%`) 
Dans le champ "typoly":
>* saisissez la valeur ‘US’ pour les lignes préalablement sélectionnées.
>* la barre d'outils en haut de la table attributaire est tout indiquée pour des mises à jour de champ simples.![selection_expression](images/barre_maj.png)

>** Créer un champ "Etat_fouille" et y indiquer les faits fouillés et non-fouillés en booléen (1/0)**
>**Corrigé pas à pas** :

>Dans le champ "Interpret":
>
>* Sélectionner les valeurs en 'NF - …'' ( à la main ou via  ![selection_expression](images/icon_select_epsilon.png):  `"Interpret" LIKE '%NF%' `) 
>* Avec la calculatrice de champ:
>    * Créer un nouveau champ "EtatFouille"
>    * inscrire la valeur `0` (non fouillé) pour les lignes préalablement sélectionnées.
>* Inverser la sélection![icon_inverser](images/icon_inverser.png), et appliquer la valeur `1` (fouillé) pour les autres valeurs

> **Autre méthode**, directement dans la calculatrice de champ, sans sélection préalable : créer un champ EtatFouille et y appliquer les valeurs correspondant à la requête suivante
>
> ```sql
> IF("interpret" LIKE 'NF%', 0, 1)`
> ```

> ![exo](images/icon_exo.png) *Exercice en autonomie* 
>
> **Objectif:** Mettre à jour le champs **"typoly"**, avec les valeurs  **'US'** et **'Fait'**
```sql
 IF("interpret" LIKE 'US%', 'US', 'fait')
```

>![exo](images/icon_exo.png) *Exercice en autonomie* 
>
>**Objectif:** Trouver une solution pour mettre à jour le champ **"interpret"**

```sql
CASE
	WHEN "interpret" LIKE 'US -%' THEN replace("interpret", 'US - ','')
    WHEN "interpret" LIKE 'NF -%' THEN replace("interpret", 'NF - ','')
    ELSE "interpret"
END
```

> Vous comprenez ici l'intérêt d’avoir des listes de valeurs dans l’inventaire, d’homogénéiser les valeurs et de penser votre système d’enregistrement en amont.


### 1.3.3. Recherche de doublons attributaires

**Jeu de données**: Obernai (F103066_poly)

**Objectif:** Recherche de doublons  de numéro d'identifiant dans la table **F103066_poly** avec 2 méthodes:

* l’extension **Group Stat**
* une **requête SQL**

#### 1.3.3.1. Recherche de doublons avec l’extension Group Stat

**Group Stat** est l’équivalent du tableau croisé dynamique dans un tableur et fonctionne sur le même principe.

Installer l'extension **Group Stat** 

>**Compter le nombre d'occurences dans un champ avec Group Stat**
>Menu *Vecteur* ⇒ Group Stat ou Cliquer sur l'icône ![Group Stat](images\iconGS.png) 
>
>1. Dans **Couches**, choisir la couche sur laquelle on veut synthétiser l’information: **F103066_poly**
>2. Glisser le champ **numpoly** dans le panneau **Lignes**
>3. Glisser le champ **numpoly** dans le panneau **Valeurs**
>4. Glisser la fonction **compter** dans le panneau **Valeurs**
>5. **Utiliser les valeurs NULL** (permet de vérifier que tous les enregistrements ont un numéro)
>6. Cliquer sur le bouton **[Calculer]**
>7. **Visualiser le résultat** et **Trier en ordre décroissant** pour visualiser les doublons ⇒ 10 doublons attributaires.

![group_stat](images/image047.png)

#### 1.3.3.2. Recherche de doublons avec  une requête SQL

On peut utiliser cette méthode en créant un virtual layer ou via le gestionnaire de BDD.

Dans cet exemple la requête:

```sql
SELECT numpoly, count(numpoly) as nombre_doublons
	FROM F103066_poly
GROUP BY numpoly
HAVING nombre_doublons > 1
ORDER BY nombre_doublons DESC
```
va renvoyer une table contenant:

- le champ "numpoly"
- le champ "nombre_doublons" contenant le décompte d’occurrences du champ "num_poly"
  - que l'on a agrégé (clause SQL `GROUP BY`)
  - si ils sont supérieurs à  1 (clause SQL `HAVING`)
  - le tout trié par ordre décroissant (clause `ORDER BY DESC`)

Nettoyer les données : supprimer les doublons.

### 1.3.4. Vérification de géométrie : Rappel

(vue dans les formations SIG 1 découverte et SIG 2 Figure)

La plupart des opérations effectuées sur des vecteurs ne fonctionnent pas sur des couches dont la géométrie n’est pas valide (auto-intersection/noeud papillon, noeud dupliqué…). Il faut donc vérifier et réparer, le cas échéant, les géométries non valides.

1. boîte de traitement ⇒ ![qgis](images/icon_qgis.png)Géométrie vectorielle ⇒ vérifier la validité
   ⇒ méthode QGIS ;

![group_stat](images/image048.png)

1. pour supprimer les nœuds en double : boîte de traitement ⇒  ![qgis](images/icon_qgis.png)Géométrie vectorielle ⇒  Supprimer les sommets en double

![group_stat](images/image049.png)

1. pour les auto-intersections (papillons), sont détectés par l’outil “vérifier la validité” mais à corriger à la main

> **Methode SQL - Virtual layer :**
> ```sql
> SELECT numpoly, ST_ISVALIDREASON(geometry) as erreur_geometrie, geometry as geom FROM F103066_poly
> WHERE ST_ISVALID(geometry) = 0
> ```

### 1.3.5. L’ordre des entités

**Jeux de données :** (Chilleurs-aux-bois) F101055_ouverture & F101055_poly

**Objectif** : faire en sorte que l’empilement des entités géométriques corresponde à la réalité stratigraphique du site. 

Gestion par un ordre numérique stocké dans un champ.
F101055_poly se rapporte aux faits archéologiques, mais le plan ne correspond pas à la réalité stratigraphique du site (faut croire le formateur sur parole !!!).
Plusieurs solutions : 

- à la main : créer un nouveau champ dans la table attributaire de la couche et rentrer l’ordre à la main. Rapide et efficace s’il y a peu de recoupements sur le site, mais laborieux et très difficile à définir si le site est complexe.
- en récupérant un ordre défini par une BdD en charge du traitement stratigraphique du site (ex : traitement sur Le Stratifiant).

Comparaison des deux couches poly brute et réorganisée :

>1. Charger le fichier **CADOC_fait_SIG.xls** dans le projet. 
>⇒ Ouvrir la table et remarquer le champ ordre. Changer l’encodage des données sources en **UTF-8**.
>2. Renommer la table CADOC_fait_SIG Feuille1 en **CADOC_fait_SIG**
>3. Depuis **F101055_poly** : faire une jointure avec **CADOC_fait_SIG**
>4. Appliquer un style categorisé sur **ordre** pour visualiser le résultat avant tri.
>  ![group_stat](images/image050.png)
>5. Avec un **virtual layer** que l'on nommera **F10155_poly_ordre**, on réorganise la table des vestiges : 

```sql
SELECT numpoly, interpret, geometry, J_periode , J_ordre FROM F101055_poly ORDER BY J_periode ASC, J_ordre DESC
```
>6. Appliquer le même style qu'au point 4 sur **F10155_poly_ordre*.
Ici, quand on catégorise sur **ordre_*, la **"strati"** est respectée.
![avantapres](images/avantapres.png)

>Note: Pourquoi décroissant (Descending) ? Ce n’est pas forcément toujours le cas, mais, dans l’exemple utilisé ici, l’ordre numérique défini se base sur la position de l’unité d’enregistrement US/Fait au sein du diagramme stratigraphique. Les faits les plus récents se trouvent en haut du diagramme, donc sur les premières lignes (le fait le plus récent, d’un point de vue stratigraphique toujours, aura le numéro un) et les plus anciens sur les dernières lignes. Dans cet exemple, l’ordre a été obtenu à l’aide de l’application [Le Stratifiant](https://abp.hypotheses.org/3965).

>Si le contexte s’y prête, l’ordre peut être défini à la main en créant un nouveau champ dans la table attributaire de la couche désirée et en rentrant les numéros à la mains.


> Note: rappel fiche résumé [sémiologie graphique](https://slides.com/archeomatic/stat1/#/semio)



**Objectif** : restituter la periodisation et la stratigraphie avec le champ **J_periode**.
>1. choisir un style **categorisé** sur le champ **periode** (valeur qualitative).
>2. utiliser par exemple la palette de degradé de rouge **reds**.
>3. classer
>4. **periode** et **strati** sont conservées grâce à la génération de numéros d'ordre et de période cohérents.

> ![group_stat](images/image055.png)

# 2. Traitement et représentation des données

Les traitements que l'on va effectuer dans ce chapitre vont être explicités:
* avec les outils de QGIS (dans ce cas nous utiliserons préférentiellement l'accès par la **boîte à outils de traitement**).
* avec des **requêtes SQL** et l'utilisation des **couches virtuelles**

Note: Les outils prédéfinis/inclus dans QGIS ne permettent pas de tout faire, c'est pourquoi il est parfois obligatoire de passer par les requêtes SQL.

## 2.1. Requêtes, sélection et affichage

###   2.1.1 Jointure à partir d'une relation de 1 à 1

Rappel sur la  jointure attributaire qui met en Jointure par les propriétés de couche > jointure
Clic droit sur le vecteur sur lequel on veut faire la jointure > Propriétés > Jointure

voir la fiche technique [Jointure Attributaire](https://formationsig.gitlab.io/fiches-techniques/jointure_att.html)

### 2.1.2. jointure à partir d'une relation de 1 à n

**Exercice** : Réaliser une carte des sépultures en faisant ressortir celles qui contiennent du verre.

**Objectif :** Exploiter une relation de 1 à plusieurs avec une requête SQL

**Jeu de données** (Esvres) F103455_poly, F103455_point



> **Préparation** : 
>
> Regarder de plus près la structure des tables attributaires des couches **F103455_poly** et **F103455_point** :
>
> * **F103455_poly :** il n'y a qu'un seul champ contenant un identifiant unique (sans doublons). C'est  **la clef primaire** : "numpoly" ![icon_pk](images/icon_PK.png)
> * **F103455_point :** un champ d’identifiant unique : "ue" ![icon_pk](images/icon_PK.png) qui est la clé primaire **MAIS** surtout un champ "FAIT" ![icon_fk](images/icon_FK.png) qui peut permettre de relier les enregistrements de cette table à la table mère F103455_poly on appelle cela **une clef étrangère**.
>
>
> ![exo_dirigé](images/icon_exo_dirigé.png)*Exercice dirigé*
>
> **Objectif**: Créer une couche virtuelle des faits qui contiennent des points correspondant à du mobilier en verre.
>
> Note: la démarche est réalisée avec le gestionnaire de BDD ![icon_BDD](images/icon_DB.png), mais elle pourrait aussi être faite en  créant une nouvelle couche virtuelle.
>
> * Ouvrir le gestionnaire de Base de données
> * Dans le panneau **Fournisseurs de données** Dérouler les couches virtuelles.
> * Vérifier les tables avec les onglets [Info] et [Table]
> * Tapez la requête SQL suivante
>
> ```sql
> SELECT  "numpoly", F103455_poly."geometry"
> FROM F103455_poly
> JOIN F103455_point 
> 	ON "numpoly" = "fait"
> WHERE "matiere" LIKE 'Verre'
> ```
> * Cliquer sur le bouton [Exécuter]
>
> 
>
> >  **Explication de code**:
> >
> > * **`SELECT` ** suivi des champs que l’on veut voir apparaître dans la table attributaire résultante c’est à dire depuis la couche mère **F103455_poly ** cad "numpoly" ainsi que sa géométrie F103455_poly."geometry" 
> >
> > **Note :** On peut ici aussi déclarer les champs de la couche fille que vous voudriez faire apparaître (par ex. "Type", "Matiere")
> >
> > **Attention :** On veut récupérer les polygones, il faut donc spécifier la table et la colonne géométrie que l’on veut retrouver comme résultat de requête: **F103455_poly**."geometry" (on aurait pu *a contrario* demander les points mais le résultat aurait été différent).
> >
> > **Attention :** S’il y a des risques d'ambiguïté dans le nom des colonnes (si les deux tables comportent un même nom de colonne) il est alors nécessaire d’utiliser la forme **nom_de_table."champ" **
> >
> > * **`FROM`**  suivi du nom de la couche mère (celle sur laquelle on fait une sélection): **F103455_poly**
> > * **`JOIN`** couche à joindre **F103455_point**
> > * **`ON`** champs de la table mère "numpoly" (clef primaire) et de la table fille "fait" (clef étrangère) sur lesquelles faire la jointure, avec le signe **`=`** entre les 2.
> >* **`WHERE`** condition de la requête `"matiere" LIKE 'Verre' `
>
> 
>
> * Pour visualiser le résultats dans le canevas cocher la case **Charger en tant que nouvelle couche**
> * Donner un nom à la couche que vous allez créer : **poly_verre**
> * Cliquer sur le bouton [Charger]




>  ![exo](images/icon_exo.png)*Exercice en autonomie* 
>
> **Objectif**: Créer une couche **poly_perle** avec les faits qui contiennent des perles.
>
> **Correction**:
>
> ```sql
> SELECT "numpoly", F103455_poly.geometry
> FROM F103455_poly
> JOIN F103455_point
> ON "numpoly" = "fait"
> WHERE "type_obj" LIKE 'Perle'
> ```



![important](images/icon_important.png)Le résultat est une couche contenant 7 fois le fait 316 : en effet à chaque fois que la condition est vérifiée dans la table fille il renvoie une ligne.
On peut éviter les redondances avec la clause **`DISTINCT`** qui doit suivre **`SELECT`** : 

```sql
SELECT DISTINCT "numpoly", F103455_poly.geometry
FROM F103455_poly
JOIN F103455_point
ON "numpoly" = "fait"
WHERE "type_obj" LIKE 'Perle'
```



>  ![exo](images/icon_exo.png)*Exercice en autonomie* 
>
>  **Objectif**: créer une couche **sepulture_verre** contenant les sépultures ayant livré du verre, et calculer le nombre de pièces en verre pour chacune.
>
>  **Démarche:**
>
>  * D’après la couche **F103455_point** , Créer une table **nb_verre_fait** contenant le nombre de pièces en verre par sépulture : (notion d’alias, des opérateurs COUNT(), GROUP BY() et ORDER BY() )
>
>  **Correction**:
>
>  ```sql
>  SELECT "FAIT", count("FAIT") as nombre_objet
>  FROM F103455_point
>  WHERE "matiere" LIKE 'Verre'
>  GROUP BY "FAIT"
>  ORDER BY "FAIT"
>  ```
>
>  
>
>  * A l'aide d'une jointure entre la couche **F103455_poly** et **nb_verre_fait**, créer la couche finale **sepulture_verre** (notion d’alias, de jointure avec JOIN ON et ORDER BY DESC )
>
>  **Correction**:
>
>  ```sql
>  SELECT p."numpoly", n."nombre_objet", p.geometry
>  	FROM F103455_poly AS p
>  JOIN nb_verre_fait AS n
>  	ON p."numpoly" = n."FAIT"
>  ```
>
>  **Résultat**:
>
>  ![table_sepulture_verre](images/image057.png)



![important](images/icon_important.png)Le même résultat aurait pu être obtenu en une seule requête en utilisant l'opérateur SQL géométrique **`ST_CONTAINS(geometrie_du_contenant, geometrie_du_contenu)`** comme condition de la jointure entre **F103455_poly** (contenant) et **F103455_point** (contenu).

*par exemple:*

```sql
SELECT DISTINCT f."geometry", f."numpoly"
	FROM F103455_poly AS f
JOIN F103455_point AS o
	ON ST_CONTAINS (f."geometry", o."geometry")
WHERE o."matiere" LIKE 'Verre'
```

Cela fonctionne mais nous avons oublié de compter le nombre d'objet en verre par fait avec la clause **`COUNT`** + **`GROUP BY`**

*ainsi:*

```sql
SELECT f."geometry", f."numpoly", count(o."fait") as nb_objet
	FROM F103455_poly AS f
JOIN F103455_point AS o
	ON ST_CONTAINS (f."geometry", o."geometry")
WHERE o."matiere" LIKE 'Verre'
GROUP BY f."numpoly"
```

*Note: on peut aussi appliquer la clause **`GROUP BY`** a un champ **geometry** par exemple dans la formule ci-dessus remplacer la dernière ligne par :

```sql
GROUP BY f."geometry"
```

![exo_dirigé](images/icon_exo_dirigé.png)*Exercice dirigé*

**Objectif**: Créer une couche virtuelle qui sera un inventaire des faits  contenant les numéros de faits et une colonne matière contenant la liste des matières d'objets contenus dans chaque sépulture *aka* une concaténation des matières d'objets.

**Correction:**

```sql
select p.num_fait, group_concat(o.matiere, ', ') as matiere
from poly as p
join (select distinct fait, matiere from point) as o
on p.num_fait = o.fait
group by p."num_fait"
```

:warning: il a fallu dans la jointure faire appel a une requête avec l'opérateur `DISTINCT`pour n'avoir qu'une seule occurrence de chaque matière.

**Correction 2:**

Il existe une autre manière (d'aucun diraient plus élégante) de mobiliser une requête que l'on utilisera dans un autre requête avec la fonction `WITH`:

```sql
WITH mat AS(select distinct fait, matiere from point order by matiere)
select p.num_fait, group_concat(m.matiere, ', ') as matiere
from poly as p
join mat as m
on p.num_fait = m.fait
group by p.num_fait
```

## 2.2 Représentations quantitatives

### 2.2.1 Représentation par maille régulière

**Jeu de données :**   F025228_Alizay.gpkg → (Alizay) F025228_emprise, F025228_point    
**Objectif :** Représenter la densité d’objets sur une surface décapée    
**Type de variable :** quantitative relative (mobilier archéo, toute matière confondue)   
**Intérêt/Analyse :** A l'échelle de l'emprise d'un chantier la visualisation des entités archéologiques (point ou polyones) n'est pas optimale nottamment quand il y a des regroupements/agrégats. L'idée est de représenter les concentrations pour par exemple, guider la prescription de zones de fouille.   


**La démarche:**

* Créer le carroyage (une grille régulière de carrés de 10m de côtés)
* Dénombrer le nombre de points par carré
* Créer un champ "densité" (nombre de points / m²)
* Faire une représentation adaptée avec un symbologie graduée adéquate

Nous allons faire cette représentation à l'aide de 2 méthodes:

* avec **les outils QGIS** mis à disposition nativement dans le logiciel
* avec **des requêtes SQL**



#### 2.2.1.1 Représentation par maille avec les outils QGIS

#### Étape 1: Création d'une grille

![etoile.png](images/icon_important.png)Une taille de la maille *idéale* permettrait de **comparer la distribution de nos points à une distribution régulière où tous les objets occuperaient le même espace**. 

Elle dépend de la surface à traiter et du nombre d’entités représentées sur cette surface. Elle peut ainsi être obtenue par la formule suivante : *côté.du.carré.en.m = √(emprise.en.m²/nombre.de.points)*

> **Exercice dirigé - Etape 1: Création de la grille**
>
> **Objectif:** Créer une grille régulière d carrés de 10m de côté
>
> * Boîte à outils de traitements → ![qgis](images/icon_qgis.png)Création de vecteur → **Créer une grille**
>   ![box_grille](images/image058.png)
>
> 
>
> * **Paramètres:**
>   * *Type de grille* : **Rectangle (polygone)**
>   * *Étendue de la grille* : Utiliser l’emprise de la couche **F025228_emprise**
>   * *Espacement horizontal* : 10 m
>   * *Espacement vertical* : 10 m
>   * *Superposition horizontale et verticale* : 0
>   * *SCR* : **2154**
>   * *Grille* : Enregistrer vers un fichier → **F025228_grille10x10** 
>   
>
>  ![box_grille](images/image059.png)



#### Etape 2: Comptage des points


> **Exercice dirigé - Etape 2: Comptage du nombre de points par carrés**
>
> **Objectif:** Compter le nombre de points par carrés
>
> * Boîte à outils de traitements → ![qgis](images/icon_qgis.png)Analyse vectorielle → **Compter les points dans les polygones**
>   ![box_grille](images/image060.png)
>
> * **Paramètres:**
>   * *Polygones* :  **F025228_grille10x10** 
>   * *Points* : **F025228_point**
>   * *Nom du champ de dénombrement* : laisser **NUMPOINTS**
>   * *Compte* : Enregistrer vers un fichier → **F025228_maille10x10** 

>   
> ![box_grille](images/image061.png)



#### Etape 3: Calcul de la densité

![important](images/icon_important.png)Une carte de densité correspond à un rapport, à savoir le nombre d’entités rapporté à une unité de surface.

On  veut créer un nouveau champ "densite" grâce à la calculatrice de champ ![img](images/icon_calc.png)correspondant au rapport du nombre d’entité par rapport à la surface de la maille.



MAIS avant...

>![id](images/icon_ampoule.png)**BONUS**
>
>On peut vouloir avant de faire le calcul, découper la maille selon l'emprise.
>On utilisera pour cela l'outil :
>
>*  Boîte à outils de traitements → ![qgis](images/icon_qgis.png)Recouvrement de vecteur → **Couper**
>* **Paramètres:**
>	* *Couche Source* : (que l'on veut découper) **F025228_maille10x10**
>	* *Couche de superposition*: (celle qui découpe) **F025228_emprise**
>	![box_grille](images/image062.png)




> **Exercice en autonomie - Étape 3: Calcul de la densité**
>
> **Objectif:**  Avec la calculatrice de champs, calculer la densité de points (en m²) dans un nouveau champ "densite" de la couche **F025228_maille10x10**
>
> *Note: chaque carré a une surface de100m²*
>
> **Correction:**
>
> ![attention](images/attention_color.png)Créer un nouveau champ / densite / Nombre décimal (réel) / Longueur = 10 / Précision = 3
>
> ```sql
> "NUMPOINTS" / 100
> ```


#### Etape 4: Représentation des densités



Faire une carte de densité c'est avant tout faire une représentation graphique d'une variable Quantitative Relative (dite aussi de rapport), on veut exprimer un ordre entre des densités faibles et des densités plus fortes. cf. la [fiche mémo en ligne de la formation stat](https://slides.com/archeomatic/stat1#/11/9)

> **Exercice dirigé - Etape 4: Représentation des densités**
>
> **Objectif:** Faire une symbologie graduée pour représenter les densités par mailles
>
> **![important](images/icon_important.png)Préalable**: Les mailles vides de matériels, et elles seules, doivent être représentées en blanc !
>
> **Paramètres**:
>
> *  Symbologie:  ![gradue](images/icon_gradue.png)Graduée
> * Palette de couleur : Choisir exclusivement un dégradé.
> *  Mode : **Ruptures naturelles (Jenks)**.
> *  Classes* : **5**
> *  Cliquer sur **Classer**
> *  Si la palette commence par un symbole à fond blanc, modifier la couleur pour la rendre très **légère** en tant que première classe du dégradé.
> * Ajouter une classe qui représentera la classe à densité = 0 
> * Appliquer à cette classe un symbole à **fond blanc**.

![important](images/icon_important.png) Pour le choix du **mode de discrétisation** (partition en classe) et le nombre de classes on se rapportera à la *formation d'Initiation aux statistiques*



>  ![exo](images/icon_exo.png)*Exercice en autonomie* 
>
>  **Jeu de données:** (Alizay) **F025228_point**, **F025228_maille_10x10** (*créée dans l'exercice précédent*)
>
>  **Objectif**: Créer une représentation par maille des densités par type de matériel: **lithique** et **céramique**
>
>  *Note: il est préférable de garder la même taille de maille pour pouvoir comparer les cartes*
>
>  **Correction-Démarche:**
>
>  * **F025228_point**: sélectionner la valeur 'Lithique' dans le champ "interpret" (58472 entités)
>  * Depuis la **Boîte de traitement**, utiliser l'outil ![icon_bt_count](images/icon_bt_count.png)**Compter les points dans un polygone**
>  * ![attention](images/attention_color.png) **Paramètres:**
>    * Ne travailler que sur les points sélectionnés en cochant la case dédiée
>    * Changer le *Nom du champ de dénombrement* en "count_lithique"
>
>  ![interface_compter_point](images/image064.png)
>
>  * Avec la **calculatrice de champs**  ![calc](images/icon_calc.png) calculer la densité dans un nouveau champ "dens_lithique"
>    ⇒ Créer un nouveau champ / densite / Nombre décimal (réel) / Longueur = 10 / Précision = 3 / `"count_lithique" / 100`
> * De la même manière que pour la valeur "Lithique", sélectionner la valeur "Céramique" dans **F025228_point**
> * En partant de la couche temporaire précedemment créée par l'algorithme **Compter les points dans un polygone**, relancer l'algorithme pour la céramique en ne travaillant que sur les entités sélectionnées (céramique)
> * Dans cette seconde couche temporaire, avec la **calculatrice de champs**  ![calc](images/icon_calc.png) calculer la densité de céramique dans un nouveau champ "dens_céramique".
>  Ainsi les densités céramique et lithique sont sur la **même** couche temporaire qu'il convient de rapidement convertir en couche permanente.
>  * Créer **un style** pour la densité céramique et **un** pour le lithique avec une **Symbologie ![gradue](images/icon_gradue.png)Graduée** (ne pas oublier d'ajouter une classe pour les valeurs 0 et de modifier les étiquettes)
>
>  ![ex_maille](images/image065.png)
>

![](images/icon_important.png)Pour comparer des cartes il faut:
* utiliser le **même nombre de classes**
* utiliser un Mode de partition en classe (= **méthode de discrétisation**) qui permet la comparaison: Nombre égal (Quantile), Ecart-type ou "à la main" (grâce à l'onglet [Histogramme])  

#### 2.2.1.2 Représentation par maille avec des requêtes SQL

On peut évidemment faire une démarche *presque* similaire  à l'aide de requêtes SQL.
Nous allons le faire ici pour le matériel 'Céramique' (champ "interpret")
A l'aide du ![DB](images/icon_DB.png)  *Gestionnaire de BDD* on va:
- Accrocher les points représentant de la Céramique à une grille régulière de points.
- Créer à partir de celle-ci, une maille carrée dans laquelle on va compter les points.



#### Étape 1: Accrocher des points à une maille régulière de points

A partir de la couche de points **F025228_point**, on va créer une grille de points régulièrement espacés de 10 m et on va raccrocher (*snap*) tous les points de la couche d'origine représentant de la céramique à ceux-ci.

```sql
SELECT p."numpoint", ST_SnapToGrid (p."geometry", 10) AS geom
	FROM F025228_point AS p
WHERE interpret LIKE 'Céramique'
```

Enregistrer la couche créée sous le nom de *grid_point*.


**Note: **remarquez que le gestionnaire de BDD à reconnu la colonne "geom "comme étant la géométrie de votre couche. Cependant elle sera automatiquement renommée "geometry" au chargement de la couche.

**Résultat:** Les 13814 points ont été *snappés* (raccrochés) à la grille régulière de points. 

**Explication SQL:**

`ST_SnapToGrid(geometry, taille_de_la_cellule)` : Cette [fonction](https://postgis.net/docs/ST_SnapToGrid.html) SQL géométrique permet de raccrocher (*snap*) :

-  les points d'une couche (1er argument de la fonction = *geometry* d'une couche de points) 
- à une grille composées de cellules carrés (2eme argument = longueur du côté de la cellule).
 ![](images/attention_color.png) la grille n'est pas créée sous forme de polygones mais d'un point au centre de la cellule.


#### Étape 2: Créer une maille carrée et compter le nombre de points

A partir de notre couche de 13814 points rassemblés sur une grille régulière de points **grid_point** , on va créer une grille de carrés de 10m de côtés et compter le nombre de points à l'intérieur.

```sql
SELECT COUNT(gp."geometry") AS count_ceram 
			, ST_Expand(gp."geometry",5) AS geom
FROM grid_point as gp
GROUP BY geom
```
Enregistrer la couche créée sous le nom de *grid_maille*

**Explication SQL:**

`ST_Expand(geometry, distance)`:  Cette [fonction] (https://postgis.net/docs/ST_Expand.html) SQL géométrique permet de créer une *bounding box* (enveloppe englobante = rectangle à partir de Xmin-Ymin & Xmax-Ymax):

-  à partir d'une couche (1er argument = *geometry*, ici les cellules créés avec le comptage des points)
-  en spécifiant de combien elle s'étend dans toutes les directions (à partir du point central vers ↑→↓← c'est pourquoi on mais comme valeur la moitié du côté de la cellule, ici 5)

#### Etape 3: Faire la représentation 

Et la le SQL ne peut rien pour vous ;)

#### Étape Bonus: La requête en *One Shot*

```sql
SELECT COUNT(p."numpoint") AS count_ceram,
	ST_Expand(ST_SnapToGrid(p."geometry", 10),5) AS geom
FROM F025228_point AS p
WHERE "interpret" LIKE 'Céramique'
GROUP BY geom
```



### 2.2.2. Cercles proportionnels

*Note: Leur mise en œuvre est  expliquée dans la formation **SIG 2 : Figures** (page 68 du déroulé stagiaire).*

**Jeu de données :** (Obernai) **F103066_ouverture.shp** et **Obernai_ceram_LTD.csv**
**Objectif :**  Représenter quantité de mobilier céramique (NR) dans les tronçons de fossés (sondages) à l'aide de cercles proportionnels.
**Type de variable :** données quantitatives de stock (absolue) (mobilier archéo, toute matière confondue)
**Intérêt/Analyse :**  La  manière souvent la plus efficace pour représenter cartographiquement une quantité absolue / de stock est d'utiliser les cercles proportionnels, et donc la variable visuelle taille.



**La démarche:**

* Préparer les données à partir de l'inventaire céramique → NR par sondages
* Joindre les données préparées à la couche de sondages 
* Modifier la géométrie de la couche obtenue (polygones) en couche de points → centroïdes
* Faire la représentation cartographique en cercles proportionnels → symbologie : Assistant Taille
* Éventuellement préparer la légende des cercles proportionnels.

Nous allons effectuer cette démarche à l'aide de 2 méthodes:

* avec **les outils QGIS**
* avec **des requêtes SQL**



#### 2.2.2.1 Cercles proportionnels avec QGIS

#### Étape 1: Préparer les données tabulées

La première étape consiste à préparer les données à quantifier d'après l'inventaire sous forme de tableau.

> **Exercice dirigé - Étape 1: préparation des données tabulées**
>
> **Objectif:** Importer le tableau CSV & agréger la variable quantitative par sondages
>
> * **Importer le tableau table/Obernai_ceramLTD.csv**
>
> ![attention](images/attention_color.png) pour **conserver le formatage des champs** du csv, **il faut l’ouvrir par le gestionnaire de source de données ![icon_gest_source_donne](images/icon_dsm.png)→ texte délimité ![icon_csv](images/icon_csv.png)**
>
> ![import_csv](images/image066.png)
>
> * **Agréger le nombre de restes par sondages**
>
> **Préalable**:
>
> * Ouvrir la table attributaire de la couche contenant les sondages **F103066_ouverture**: on constate que les sondages ont bien un numéro unique composé du numéro de fait (fossé) et du numéro de sondage dans un champ "numouvert" *(exemple, numouvert = 100120, pour fossé 1001 et sondage 20)*.
> * En ouvrant la table attributaire de **Obernai_ceramLTD,** on constate:
>   *  qu'il n'y a pas de champ avec un identifiant unique
>   * que le mobilier est enregistrée en "NR" par lots
>   * que les sondages sont  enregistrés/codés dans le champ "StSd" comme dans le champ "numouvert" de la couche F103066_ouverture.
>
> **Objectif:** Agréger les nombre de restes par lots à l'aide de l’extension **Group Stats** ![group_stat](images/iconGS.png)
>
> ![group_stat](images/image067.png)
> * **Paramètres:**
>
>   1. Choisir la couche : **Obernai_ceram_LTD**
>
>   2. *Lignes* : **StSd**
>
>      *Valeurs* : **NR** et la fonction **somme** 
>
>   3. [Calculer]
>
>   4. Enregistrer le résultat en **CSV** 

Le résultat est une table contenant le nombre de restes (NR) de céramiques par sondages (StSd).

#### Étape 2: jointure entre les données et la couche de sondages

Il faut maintenant joindre cette table à une couche géométrique pour pouvoir cartographier cette quantité de céramique par tronçon de fossés.

>**Exercice dirigé - Étape 2: jointure**
>
>* Ouvrir le tableau contenant les NR par sondages depuis le gestionnaire de source de données ![icon_gest_source_donne](images/icon_dsm.png)→ texte délimité ![icon_csv](images/icon_csv.png)
>
>* Faire la jointure de la table csv depuis la couche **F103066_Ouverture**: 
>
>![jointure](images/image068.png)

La couche F103066_ouverture contient toutes les données nécessaires a cartographier, cependant c'est une couche de polygones or nous voudrions représenter des cercles proportionnels c'est à dire partir de **symboles ponctuels**.

#### Étape 3: Changement de géométrie des sondages: centroïdes

Nous allons pour cela créer une couche de centroïdes des polygones.

> **Exercice dirigé - Étape 3: centroïdes**
>
> On peut accéder à l'outil dédié dans QGIS:
>
> * par le menu **Vecteur → Outils de géométrie → Centroïdes..**
> * par le menu **Traitement → Boîte à outils → ![qgis](images/icon_qgis.png)Géométrie vectorielle → Centroïdes** 
>
> ![centroides](images/image069.png)



Nous disposons maintenant d'**une couche de points au barycentre des tronçons de fossés et contenant la somme des NR de mobilier céramique** !

#### Étape 4: Représentation en cercles proportionnels avec l'Assistant Taille

La représentation d'un symbole ponctuel selon une taille proportionnelle à une donnée quantitative est disponible dans les propriétés de la symbologie de QGIS.

> **Exercice dirigé - Étape 4: l'Assistant Taille**
>
> * Accéder à la fenêtre symbologie de la couche de centroïdes nouvellement créée.
>
> ![symbologie](images/image070.png)
>
> 1. Choisir **Symbole unique**
> 2. Pour le paramètre **Taille**, cliquer sur l’icône de *Définition de données imposée* ![icone_expression](images/icon_def.png) et choisir l’option **Assistant...**
>
> ![assistant_taille](images/image071.png)
>
> * Dans la fenêtre de l'Assistant Taille:
>   1. *Source* (le champ à représenter): "_None"
>   2. Récupérer automatiquement les valeurs minimales et maximales du champ grâce au bouton ![iconF5](images/icon_F5.png) 
>   3. la *Méthode de calcul* doit être **Surface**
>   4. On peut éventuellement choisir la taille minimale et maximale des cercles en sortie
> * **Bonus: ** Paramétrer l'Ordre de rendu des couches pour afficher les petits cercles au dessus des gros:
>
> ![ordre_de_rendu](images/image074.png)
Voilà !! une représentation juste et efficace du nombre de restes de céramiques par tronçon de fossés !

Cependant pour compléter cette représentation il faut pouvoir afficher ces cercles proportionnels en légende...

#### Étape 5: Légende des cercles proportionnels

> **Exercice dirigé - Etape 5: légende des cercles proportionnels**
>
> * Il faut revenir à la fenêtre principale de la symbologie de la couche **Centroïdes**
> * Bouton [Avancé] → Légende définie par la taille des données
>
> ![avancé](images/image072.png)
>
> * Il ne reste plus qu'à paramétrer la légende des cercles proportionnels.
>
> ![legende](images/image073.png)

![cercle_prop](images/image075.png)

#### 2.2.2.2 Préparation des données  en SQL pour une représentation en cercles proportionnels

Toute la partie concernant la préparation des données (agrégation  des NR par sondage) et les manipulations entre les tables (jointure) ainsi que le changement de géométrie (centroïdes) peuvent être effectuées grâce à des/une requête SQL.

Reste à QGIS toute la partie concernant la représentation graphique (paramétrage de l'assistant taille et de la légende).

#### Étapes par étapes : agrégation → centroïdes → jointure

 A l'aide du ![DB](images/icon_DB.png)  *Gestionnaire de BDD*  depuis les ![miniqgis](images/icon_qgis.png)Couches du projet:

  * Depuis la table Obernai_ceram_LTD, il faut faire une table contenant la somme des NR par StSd 
```sql
SELECT "StSd", SUM("NR") AS somme_nr
FROM Obernai_ceram_LTD 
GROUP BY "StSd"
ORDER BY "StSd"
```
→ cocher *Charger en tant que nouvelle couche* (sans géométrie) , *Nom de la couche* = **sum_nr**, [Charger]
  * Depuis la  couche F103066_ouverture, créer une couche de de centroïdes
```sql
SELECT "numouvert", "typouvert", ST_centroid(o.geometry)
FROM F103066_ouverture AS o
```
→ cocher *Charger en tant que nouvelle couche* , *Nom de la couche* = **sd_centro**, [Charger]
  * Faire une jointure des centroïdes d'ouverture (ici sd_centro ) avec la table contenant la somme des NR (ici sum_nr)

```sql
SELECT sd."numouvert", sd."geometry", nr."somme_nr"
FROM sd_centro AS sd
JOIN sum_nr AS nr
	ON sd."numouvert" = nr."StSd"
```
→ cocher *Charger en tant que nouvelle couche* , *Nom de la couche* = **sd_sum_nr**, [Charger]

#### SQL *One Shot*
Évidemment quand on a compris la démarche et que l'on sait les formaliser en SQL il est possible de le faire en une seule requête:
```sql
SELECT o."numouvert", o."typouvert", sum("NR") as somme_nr /*:int*/ , st_centroid(o."geometry") AS geometry /*:point:2154*/
FROM F103066_ouverture AS o
JOIN Obernai_ceram_LTD as c
	ON "numouvert" = "StSd"
GROUP BY "StSd"
```

→ cocher *Charger en tant que nouvelle couche* , *Nom de la couche* = **sd_sum_nr**, [Charger]



Ne reste plus qu'à paramétrer la symbologie de la couche **sd_sum_nr** grâce à l'**Assistant taille** !



> ![exo](images/icon_exo.png)**Exercice en autonomie:** A partir du même jeu de données, représenter le poids total de mobilier céramique ("Poids") dans les tronçons de fossés (sondages) à l'aide de cercles proportionnels.

#### :skull: FIN DU DEUXIEME JOUR :skull:

## 2.3 Analyses et représentation spécifiques

### 2.3.1 Représentation en oursin

La représentation en oursin est une représentation cartographique appelé aussi diagramme origine-destination. Elle est couramment utilisée en archéologie pour représenter la diffusion d'une matière première (silex, céramique,etc).

A ne pas confondre avec les oursons ! :bear:

![ourson](images/ourson.gif)



**Jeu de données :** (Obernai) F103066_poly.shp et Obernai_lithique_LTD.csv  (à ouvrir avec  ![csv](images/icon_CSV.png)  pour garder le format des colonnes)

**Contexte :** La fouille d’Obernai a permis de mettre en évidence la fabrication de meules en grès et en rhyolite sur le site à La Tène finale. Des ébauches et des éclats de taille ont été découverts.

**Objectifs :** 

* Représenter par des lignes les relations entre des fosses ayant livré des ébauches de meules et celles dans lesquelles ont été découverts des éclats de taille.
* Deux types de roches ont été utilisées (rhyolite et grès), il faudra les représenter par des couleurs différentes, en fonction du type de roche.

**Type de variable :** données qualitatives (origine , destination et matière)



#### Étapes par étapes: Faire un diagramme en oursin



> **Faire un diagramme en oursin** (Exercice dirigé)
>
> **Préalable**: 
>
> * Consulter les tables F103066_poly et Obernai_lithique_LTD pour trouver un champ commun pour la jointure → "numpoly" et "ST"
> * Dans la table Obernai_lithique_LTD, identifier les champs permettant de relier la structure d'**origine = ébauche de meule** et la structure de **destination = éclats de tailles** → "ST" et "Origine"
>
> **Démarche**:
>
> * A partir de la couche F103066_poly, créer une couche de centroïdes de la couche → **poly_centro**
>
>   * QGIS: menu **Traitement → Boîte à outils → Géométrie vectorielle → Centroïdes** 
>
>   * SQL:
> ```sql
>     SELECT "numpoly", "typoly", "interpret", "datedebut", "datefin", st_centroid("geometry") AS geometry
>     FROM F103066_poly
> ```
>
> * Joindre à cette couche de centroïdes la table d'inventaire Obernai_lithique_LTD → **poly_centro_lith**
>
>   * QGIS: Jointure avec les champs communs "numpoly" et "ST"
>   * SQL:
>
>   ```sql
>   SELECT * 
>   FROM poly_centro
>   JOIN Obernai_lithique_LTD
>   	ON "numpoly" = "ST"
>   WHERE "Origine" IS NOT NULL
>   ```
>
>   Note: On a ajouté dans la requête une clause `WHERE`pour prendre en compte uniquement les enregistrements contenant une valeur dans le champs "Origine" c'est à dire dont la meule d'origine de l'éclat de taille est connu ! Question de performance également. (Ce critère peut être aussi appliqué au moment de la requête de création des oursins...mais pas deux fois!)
>
>
> * Tracer le diagramme en oursin reliant le centroïde de la structure de l'éclat à celui de la structure de la meule d'origine.
>
> ![attention](images/attention_color.png)Cette démarche n'est ici proposée qu'en SQL, les outils de QGIS permettant de faire la même chose n'étant pas stable au moment de la rédaction de ce support (notamment l'extension MMQGIS → HubLines)
>
> On utilise la fonction `make_line(p1.geometry, p2.geometry)` qui permet de tracer une polyligne entre deux géométries de points. 
>
> Pour cela,  A l'aide du ![DB](images/icon_DB.png)  *Gestionnaire de BDD*  depuis les ![miniqgis](images/icon_qgis.png)Couches du projet:
>
> *  on appelle la couche de centroïdes jointe de l'inventaire lithique (et donc qui contient le champ "Origine") qui servira de p1 = **poly_centro_lith**
> *  on y joint la couche de centroïde **poly_centro** alias p2 en mettant en relation les champs p2."numpoly" (structure de destination des éclats) au champ p1."Origine" (structure d'Origine de la meule)
>
> ```sql
> SELECT  make_line(p1."geometry", p2."geometry") as geometry,
> 	   p1."Matiere", p1."Masse" 
> FROM poly_centro_lith AS p1
> JOIN poly_centro as p2
> 	ON p2."numpoly" = p1."Origine"
> <!-- where j_"Origine" IS NOT NULL --!>
> ```



#### SQL *One Shot*

On peut aussi tout faire (les centroïdes, la jointure avec la table et "l'autojointure") en une seule requête SQL :

```sql
SELECT make_line(centroid(p1."geometry"),centroid(p2."geometry")) AS geometry,   p1."numpoly",  p1."Origine",  p1."Matiere",  p1."Masse"
FROM (F103066_poly
      JOIN Obernai_lithique_LTD ON "numpoly" = "ST") AS p1
JOIN (F103066_poly
      JOIN Obernai_lithique_LTD	ON "numpoly" = "ST") AS p2
	ON p1."Origine" IS NOT NULL AND p2."numpoly" = p1."Origine"
```

→ cocher *Charger en tant que nouvelle couche* , *Nom de la couche* = **oursin_meule**, [Charger]



![atention](images/attention_color.png)Pour limiter les plantages, quand le résultat risque d'être réutilisé il est conseillé de l'enregistrer.



#### Représentation graphique

> ![exo](images/icon_exo.png)**Assigner une couleur à chaque type de roche utilisé** (*Exercice en autonomie*)
>
> **Correction**: 
>
> * Symbologie Catégorisée selon champ "Matiere"
> * Bouton [Classer]
> * Modifier les couleurs
>
> ![flux_categ](images/image076.png)



> ![exo](images/icon_exo.png)**Assigner un poids à chaque relation en fonction du champ "Masse"** (*Exercice dirigé*)
>
> **Démarche:** 
>
> * A partir de la fenêtre Symbologie avec la catégorisation précédente :
> * Cliquer sur le symbole linéaire
> * Dans l'option *Largeur* cliquer sur l'icône  de *Définition de données imposée* ![icone_expression](images/icon_def.png) et choisir l’option **Assistant...**
> *  **Paramètres**:
>   * *Source*: "Masse"
>   * Récupérer automatiquement les valeurs min et max grâce au bouton ![iconF5](images/icon_F5.png)
>   * *Méthode de calcul*: **Linéaire**
>   * *Sortie Taille depuis* **1** *à* **6**
>   * Bouton [OK]
> * **Bonus** :   pour parfaire la représentation il faut modifier la forme des extrémités des lignes, dans le style de la couche changer le *Style de cap* à **Plat**  ![cap](images/image077.png)
>
> ![flux_poids](images/image078.png)



### 2.3.2 Recollage de céramique

Il est courant en archéologie de faire du remontage d'objets (silex, céramique,etc) et il est parfois utile de représenter les liens de remontages à partir d’une table origine-destination et d’une couche de points, pour peu que tous les éléments ayant servis au remontage aient été localisés. 



**Jeu de données** : (Esvres) F103455_point.shp et S324_3202.csv

**Contexte :** nécropole du début de notre ère avec des sépultures contenant rarement des os à cause de l’acidité du sol mais beaucoup de mobilier.

**Problématique/Objectif** : Un des objectifs de cette fouille était de travailler sur l'éparpillement du mobilier afin de s'interroger sur les phénomènes taphonomiques et/ou les gestes funéraires qui ont amené à l’état du mobilier lors de sa découverte. Tous les tessons ont donc été pris en topo (x,y,z et identifiés par un numéro d’UE). Le céramologue a créé des tableaux de remontage (table de jonction) des tessons avec un champ “start” et un champ “end” indiquant les différents liens de remontage. L’objectif ici est de représenter spatialement ces liens de remontage.

**Type de variable :** données qualitatives (origine , destination)



> **Représenter les liens de remontages à partir d’une table origine-destination et d’une couche de points** (Exercice dirigé)
>
> **Préalable**:
>
> On dispose :
>
> * d’une couche de points **F103455_point.shp** qui contient le numéro d’identifiant "UE"
> * d’une table de jonction **S324_3202.csv** qui contient les liens de remontage entre les tessons d’une même céramique avec une colonne "start" et une colonne "end" qui contiennent les numéros d’identifiants "UE" 
>
> **Démarche**:
>
> Faire une requête SQL permettant:
>
> * de tracer des lignes grâce à la fonction `make_line(p1.geometry, p2.geometry)`
> * à partir du tableau de remontage dans lequel on va récupérer à partir des points, la géométrie pour les ue de la colonne start et la géométrie pour les ue de la colonne end, soit réaliser une double jointure entre S324_3202.csv et F103455_point :
>   * une fois sur les champs "UE" de la couche et "start" de la table de remontage.
>   * une 2ème fois sur les champs "UE" de la couche et "end" de la table de remontage.
> * **La Requête**, à l'aide du ![DB](images/icon_DB.png)  *Gestionnaire de BDD*  depuis les ![miniqgis](images/icon_qgis.png)Couches du projet:
>
> ```sql
> SELECT p1."UE" AS pt_depart,  p2."UE" AS pt_arrivee,
>        make_line(p1."geometry", p2."geometry") AS geometry
> FROM S324_3202 AS t
>      JOIN F103455_point AS p1 ON t."start" = p1."UE"
>      JOIN F103455_point AS p2 ON t."end"   = p2."UE"
> ```
>
> ![emontage](images/image079.png)





> ![exo](images/icon_exo.png)**Représenter les liens de remontages à partir de la table de jonction S324_3203** (*Exercice en autonomie*)
>
> **Correction**: 
> 
> * Ajouter la table S324_3203 au projet (pas besoin de passer par ![csv](images/icon_csv.png) car tous les champs sont en texte)
> * Recopier la requête SQL est remplacer `S324_3202` par `S324_3203`

### 2.3.3 Calcul d'orientation

Il est courant de travailler sur les orientations de vestiges archéologiques (sépultures, fossés, parcelles, etc).

 A partir de lignes il est aisé de le faire dans QGIS[^5]:
[^5]: Nous n'abordons pas ici les calculs d'orientations à partir de polygones. ll existe une méthode basée sur la création d'emprises orientées minimales (*Minimal Oriented Bounding Box*). voir dans Menu [Traitement] → Boîte à outils → ![miniQ](images/icon_qgis.png) Géométrie vectoriel → Emprise orientée minimale MOBB
* avec des fonctions de géométries inclues dans la **calculatrice de champs** ![calc](images/icon_calc.png)

* avec une requête SQL

**Jeu de données** : (Esvres) F103455_axe.shp

**Contexte :** nécropole du début de notre ère avec des sépultures contenant rarement des os à cause de l’acidité du sol mais beaucoup de mobilier.

**Problématique/Objectif** : Explorer les orientations en degrés par rapport au Nord des sépultures d’après les axes de coupes matérialisés sur le terrain.

**Type de variable :** on veut récupérer une donnée quantitative repérable en degré.



**Préalable**:

* On dispose d’une couche de polyligne correspondant aux axes de coupes**F103455_axe** qui contient le numéro d’identifiant "fait" ainsi qu’un champ “typaxe” contenant le type d’axe ('Lon' si c’est un axe longitudinal, 'Tra' s’il est transversal).
* On peut visualiser le sens de numérisation/prise de points topo en appliquant un style avec des flèches.

![fleche](images/image080.png)



#### 2.3.3.1 Calcul d'orientation avec la calculatrice de champs

> **Calcul d'orientation avec la calculatrice de champs** (Exercice dirigé)
>
> On va à l’aide de la calculatrice de champ récupérer l’orientation des axes (d’après la position du point de départ et celle du point de fin de l’axe) par rapport au Nord.
>
> **Démarche**:
>
> * Ouvrir la calculatrice de champs ![calc](images/icon_calc.png)
> * Créer un nouveau champ "azimuth" de type entier
> * Dans la fenêtre expression, taper:
>
> ```sql
> degrees( azimuth( start_point($geometry), end_point($geometry)	) )
> ```
>
> * [OK]
>
> **Détail de la fonction**:
>
> * fonction **Math** → `degrees()`: convertit des angles en radians vers des degrés.
>
> * fonction **Géométrie** → `azimuth(point a, point b)`: renvoie l'azimut par rapport au nord sous forme d'angle en radians mesuré dans le sens des aiguilles d'une montre à partir de la verticale entre point_a et point_b.
>
> * fonctions **Géométries** → `start_point`et `end_point` : renvoient respectivement le premier et le dernier nœud d'une géométrie.
>
>   *Note: l'ordre de numérisation ou de prise de points topo à donc une importance quand on veut travailler sur les orientations*
>
> * fonction **Géométrie** → `$geometry`: renvoie la géométrie de l'entité courante. Cette fonction peut être utilisée en combinaison avec d'autres fonctions.



#### 2.3.3.2 Calcul d'orientation avec une requête SQL

Les fonctions de la calculatrices de champs ne sont rien de plus que l'implémentation de fonctions SQL dans cette outil Il est donc évidemment possible d'utiliser ses fonctions directement avec une requête SQL.

>  **Calcul d'orientation avec la calculatrice de champs** (Exercice dirigé)
>
>  à l'aide du ![DB](images/icon_DB.png)*Gestionnaire de BDD*  depuis les ![miniqgis](images/icon_qgis.png)Couches du projet:
>
>  ```sql
>  SELECT "fait" as numpoly, "geometry",
>  	degrees(azimuth(st_startpoint(geometry),st_endpoint(geometry))) as azimuth
>  FROM F103455_axe
>  WHERE "typaxe" LIKE 'Lon'
>  ```



**Notez que**:

* l'on appelle le champ "geometry" pour obtenir une couche de polyligne en sortie
* que l'on a limité les résultats aux axes longitudinaux avec la clause `WHERE`
* que l'on en profite pour renommer le champ "fait" en "numpoly" avec l'utilisation d'un alias



> ![](images/icon_exo.png)**Calculer l'orientation des axes des sépultures de la fouille d'Obernai et proposer une représentation graphique** (*Exercice en autonomie*)
>
> **Jeu de données**: (Obernai) vecteur_operation/F103066_axe_sep.shp



### 2.3.4 Création d'un profil d'après un MNT

Avec l'usage de plus en plus trivial de la photogrammétrie sur les opérations archéologiques nous pouvons disposer de Modèles Numériques de Terrains (MNT) à l'échelle de la structure voire du chantier. Il peut être utile d'utiliser cette documentation pour en tirer des profils *a posteriori*.

QGIS dispose nativement de tous les outils nécessaires.



**Jeu de données**: (Esvres) F103455_S324_MNT_Fond.tif et F103455_axe.shp

**Objectif:** créer un profil de fond de la sépulture S324 d’après un axe longitudinal (**F103455_axe.shp**) et un MNT issu d’un relevé photogrammétrique (**F103455_S324_MNT_Fond.tif**).



> **Créer un profil le long d'un axe d'après un MNT** (Exercice dirigé)
>
> **Démarche**:
> * Créer une couche de points le long de l'axe avec la valeur Z issue du MNT
> * Reprojeter ces points dans le long de l'axe
> * Relier ces points pour obtenir un profil en polyligne
>
> 
>
> ![attention](images/attention_color.png)Il est conseillé à chaque création de couche de vérifier la bonne éxecution du traitement:
>
> *  en ouvrant la table attributaire (vérification attributaire)
> * en faisant un clic droit → Zoomer sur la couche (vérification spatiale)
>
> 
>
> 1. Charger le raster **F103455_S324_MNT_Fond.tif**
>
> 2. Sélectionner graphiquement ![icon_select](images/icon_select.png) l’axe longitudinal de la sépulture S324 dans la couche **F103455_axe.shp**
>
> 3. Créer une couche de points avec des champs: "DIST" (distance au points 0 de l’axe) et Z (valeur de Z récupérée sur le MNT): 
>
>    Dans la Boîte à outils de traitements → ![saga](images/icon_saga.png)SAGA → Terrain Analysis - Profiles → ![saga](images/icon_saga.png)**Profiles from lines**
>
>    ![profil_from_line](images/image081.png)
>
>    > Paramètres:
>    >
>    > * *DEM*: F103455_MNT_324_Fond
>    > * *Lines*: F103455_axe
>    > * **Cocher** *Entité(s) sélectionnée(s) uniquement*
>    > * [Exécuter]
>    >
>    > → *couche temporaire*  de points**PROFILES**
>
> 4. Reprojeter ces points dans un axe DIST / Z : 
>
>    Dans la Boîte à outils de traitements → ![qgis](images/icon_qgis.png)Création de vecteurs →  ![trait](images/icon_trait.png) **Créer une couche de points à partir d’une table**
>
>    ![point_table](images/image082.png)
>
>    > Paramètres:
>    >
>    > * *Couche en entrée*: PROFILES
>    > * *Champ X*: **DIST**
>    > * *Champ Y*: **Z**
>    > * *SCR cible*: EPSG2154
>    > * [Exécuter]
>    >
>    > → *couche temporaire* de points **Points depuis une table**
>
> 5. Relier les points projetés pour faire un profil : 
>
>    Dans la Boîte à outils de traitements → ![saga](images/icon_saga.png)SAGA →   Vector line tools →  **Convert points to line(s)**
>
>    ![convert_point_to_line](images/image083.png)
>
>    > Paramètres:
>    >
>    > * *Points*: Points depuis une table
>    > * *Order by...*: **DIST**
>    > * [Exécuter]
>    >
>    > → *couche temporaire* de ligne **LINES**
>
>    ![profil_mnt](images/image084.png)



Note: le RGE Alti 1m peut bien entendu être exploité avec cette méthode !



> ![exo.png](images/icon_exo.png)**Avec le modeleur graphique, créer un modèle contenant ces 3 traitements à la suite** (Exercice en autonomie) *facultatif*



### 2.3.5 Reprojection de points sur un axe

Alors que nous relevons régulièrement du mobilier dans les 3 dimensions x,y,z *a priori* QGIS ne nous permet de ne le visualiser qu'en 2 dimensions. Dans certains cas il peut être intéressant de visualiser ces mobiliers "en coup" c'est à dire en les **reprojetant selon un axe**. L'exemple ci-dessous concerne de la céramique mais est applicable pour tout type de matériel topographié précisemment en x,y et z (silex par ex.) .

Ce traitement est uniquement possible en utilisant une requête SQL exploitant des opérateurs géométriques.



**Jeu de données**: (Esvres) F103455_point.shp et F103455_axe

**Contexte:** nécropole du début de notre ère avec des sépultures contenant rarement des os à cause de l’acidité du sol mais beaucoup de mobilier.

**Objectif:** reprojeter les points de mobilier selon un axe pour avoir une vue de profil de la localisation du mobilier dans la tombe. 



> **Reprojeter des points de mobilier d’une sépulture selon un axe** (Exercice dirigé)
>
> **Préalable**:
>
> Il faut faire attention à la direction de l'axe car la reprojection des points dépendra de l’origine de l’axe ! Pour le vérifier il est possible d'appliquer un style avec un flèche comme expliqué  pour le [calcul d'orientation](#233-calcul-dorientation)
>
> **Principe**:
>
> Créer une table contenant les coordonnées qui serviront à afficher les points reprojetés dans un repère orthonormé avec:
>
> * en abscisse la distance de chaque point à l’origine de l’axe appelé **xrel**
> * en ordonnée l’altitude du point contenue dans le champ "Z"
>
> **Démarche**:
>
> * Créer une table avec:
>   *  les champs d'identification des points "fait", "ue" et "matiere"
>   * "Xrel", la distance du point projetés par rapport au début de l'axe
>   * "Z", altitude du point déjà enregistré dans la table d'origine
> * Créer les points reprojetés avec "Xrel" en abscisse et "Z" en ordonnée.
>
> 
>
> 1. Créer une table avec 
>
> ```sql
> SELECT p."fait",p."ue", p."matiere",  st_Line_Locate_Point(a."geometry",p."geometry")*st_length(a."geometry") AS xrel, p."z"
> FROM F103455_axe AS a, F103455_point AS p 
> WHERE a.fait='324' 
> 	AND a.typaxe='Lon' 
> 	AND p.fait=324
> ```
> → cocher *Charger en tant que nouvelle couche* , *Nom de la couche* = **tab_reproj**, [Charger]
>
> > * **"xrel"** est un champ que l’on crée qui correspond à la distance de chaque point à l’origine de l’axe que l’on obtient grâce aux fonctions géométriques SQL: 
> >
> > - `st_line_locate_point (a.geometry, p.geometry)` qui retourne un nombre entre 0 et 1 représentant la distance du point le plus proche (projeté sur la ligne) à l’origine de la ligne sous la forme d’une fraction du total.
> >
> > Comme on obtient une fraction on va multiplier le résultat par la longueur de la ligne pour obtenir une mesure grâce à la fonction
> >
> > - `st_length (a.geometry)` qui retourne la longueur de la polyligne
>
> 2. Création des points projetés avec X = Xrel et Y = Z
>
> ```sql
> SELECT "fait", "ue", "matiere", make_point("xrel","z") AS geometry
> FROM tab_reproj
> ```
>
> → cocher *Charger en tant que nouvelle couche* , *Nom de la couche* = **S324_point_reproj**, [Charger]
>
> > `make_point(x, y)` qui permet de tracer des points à partir de coordonnées "x" et "y"
>
> ![reproj_point](images/image085.png)



> ![exo.png](images/icon_exo.png)**Créer la requête SQL qui permet de reprojeter des points sur un axe** (Exercice en autonomie) *facultatif*
>
> **Correction**: Requête en *One Shot*:
>
> :warning: ne pas utiliser `SELECT *` mais bien indiquer les champs voulus, sinon QGIS n'affiche pas les points

```sql
SELECT p."fait",p."ue", p."matiere",
make_point(ST_Line_Locate_Point(a."geometry",p."geometry")*st_length(a."geometry"), p.z) AS geometry 
	FROM F103455_axe AS a, F103455_point AS p 
	WHERE a."fait"=324
		AND a."typaxe"='Lon' 
		AND p."fait"=324
	```

> ![exo.png](images/icon_exo.png)**Appliquer une symbologie adaptée et proposer une mise en page alliant plan et coupe** (Exercice en autonomie)

>![exo.png](images/icon_exo.png)**créer un profil et projeter les objets de la tombe 325 selon son axe longitudinal** (Exercice en autonomie)
>**Jeu de données**: (Esvres) F103455_axe.shp, F103455_point.shp et F103455_MNT_325_Fond.tif



|Bravo ! vous êtes un|
|:-:|
|![SQL_averti](images/image001.png)|



## 3. Exercices finaux

Ouvrir le projet **00_Alizay.qgz**



### Exercice 1: Collections de cartes de densités de mobilier

**Jeu de données** : (Alizay) F025228_point et F025228_Emprise

**Objectif**: Élaborer une collection de cartes montrant les densités (en NR/m²) de mobilier. 

* La  maille se composera de carrés de 10 m de côté.doit être découpée selon l’emprise.
* Il faudra effectuer 5 cartes: carte générale (densité de tous les mobiliers), carte de densité de la céramique, des charbons, du lithique et de la faune.



----------------------------------------

### Exercice 2: Atlas des mobiliers projetés sur des axes

**Jeu de données** : (Alizay) F025228_point et F025228_axe

**Objectif**: Préparer un atlas montrant la projection des points de mobilier sur les axes correspondant au locus auxquels appartiennent les objets.

