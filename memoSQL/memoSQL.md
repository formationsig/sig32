# Do you speak SQL ?

[source](http://www.commentcamarche.net/contents/1062-le-langage-sql)

le **SQL** (*Structured Query Language*, traduisez *Langage de requêtes structuré*) est un **langage de définition de données**, **un langage de manipulation de données** et un langage de contrôle de données pour les bases de données relationnelles.

Le SQL est un **langage de définition de données**, c'est-à-dire qu'il permet de **créer des tables dans une base de données relationnelle**, ainsi que d'en **modifier** ou en **supprimer**.

Le SQL est un **langage de manipulation de données**, cela signifie qu'**il permet de sélectionner, insérer, modifier ou supprimer des données dans une table d'une base de données relationnelle**. 

Il est possible avec SQL de définir des permissions au niveau des utilisateurs d'une base de données.

> En fait, le langage SQL peut rendre tous les services que l’on peut demander à une BDD relationnelle: Créer la structure de la BDD (créer des tables, créer les champs, remplir les données mais aussi créer les liens entre les tables et SURTOUT interroger la BDD (C’est d’ailleurs essentiellement pour cela que nous allons l’utiliser). C’est ce que vous savez déjà tous faire (plus ou moins) sans forcément vous en rendre compte.

Toutes les BDD relationnelles (“dignes de ce nom”) comprennent le langage SQL.

![Attention](images/attention.png) Pour connaître toutes les fonctions SQL il est utile de se référer au site [SQL.sh](https://sql.sh/)

Avant tout le SQL est donc un langage de requête structuré il respecte donc **une syntaxe générale** de type :

```sql
SELECT (liste des attributs) FROM (liste des tables) WHERE (Conditions)
```

* La partie `SELECT` indique les champs qui doivent apparaître dans la réponse. 

  *Note: ces champs sont soit des champs existants dans les tables déclarées derrière la clause `FROM` soit le résultat d’un calcul. Il faudra dans ce cas leur donner un nom.*

* La partie `FROM` décrit les tables qui sont utilisées dans la requête. 

* La partie `WHERE` exprime les conditions, elle est optionnelle.

> Quand vous faites une sélection dans QGIS la fenêtre expression correspond aux conditions c’est à dire ce qui suit la clause `WHERE`. Il faut imaginer que le logiciel fait précéder votre expression de la phrase:
>     `SELECT * FROM (Couche_en_cours) WHERE`    
>         *Où `*`signifie tous les champs*.

Il est conseillé lors de la rédaction d'une requête SQL de commencer par écrire la syntaxe générale et de commencer par déclarer les tables qui vont être utilisées derrière la clause `FROM` , puis de déclarer les champs à utiliser/créer derrière la clause `SELECT` et enfin éventuellement de spécifier les conditions derrière la clause `WHERE`.

## Les clauses SQL
(clause = Partie d'un ordre SQL précisant un fonctionnement particulier)

| clause SQL | Description |
|:----------:|-------------|
| `SELECT`   | Précise les colonnes qui vont apparaître dans la réponse |
| `FROM`     | Précise la (ou les) table intervenant dans l’interrogation |
| `WHERE`    | Précise les conditions à appliquer sur les enregistrements. On peut utiliser :|
||  - des opérateurs de comparaison `=` `>` `<` `=` `<>`|
||  - des opérateurs logiques `AND` `OR` `NOT`|
||  - les prédicats `IN` `LIKE` `NULL` `ALL` `SOME` `ANY``EXISTS`...|
| `GROUP BY` | Précise la (ou les) colonne de regroupement |
| `HAVING`   | Précise la (ou les) condition associée à un regroupement |
| `ORDER BY` | Précise l’ordre dans lequel vont apparaître les lignes de la réponse : |
|| `ASC` : en ordre croissant |
|| `DESC` : en ordre décroissant |
| `LIMIT (n)`| Permet de limiter le calcul au *n* premiers enregistrements |
| `DISTINCT` | Permet d’éviter les redondances dans les résultats (il s’agit d’un option à la commande `SELECT`). |
| `COUNT()`  | Permet de compter le nombre d’enregistrements dans une table. |

## Les opérateurs de comparaisons

La clause `WHERE` est définie par **une condition qui s'exprime à l'aide d'opérateurs de comparaison et d'opérateurs logiques**.

| opérateur | description |
|:---------:|-------------|
| A `=` B | A égal B |
| A `LIKE` 'chaîne' | permet d’insérer des caractères jokers: |
|| `%` désignant 0 à plusieurs caractères quelconques |
|| `_` désignant un seul caractère |
| A `<>` B | A différent de B |
| A `<` B | A plus petit que (inférieur à) B |
| A `>` B | A plus grand que (inférieur à) B |
| A `<=` B | A inférieur ou égal à B |
| A `>=` B | A supérieur ou égal à B |
| A `BETWEEN` B `AND` C | A est compris entre B et C |
| A `IN` (B1, B2,...) | A appartient à liste de valeur (B1,B2,..) |

## Les opérateurs logiques

`OR` pour séparer deux conditions dont au moins une doit être vérifiée.  
`AND` pour séparer deux conditions qui doivent être vérifiées simultanément.  
`NOT` permet d'inverser une condition.  

![shéma AND OR NOT](images/and_or_not.png)

# le SQL dans QGIS

Dans QGIS le SQL est partout ! On l'utilise :
  * Pour faire une sélection par expression
  * Dans la calculatrice de champs
  * Dans le *Calculateur d'expression* ![epsilon](images/epsilon.png) éditer un style *Ensemble de règles* ou des *Étiquettes*

## Le gestionnaire BD

Dans QGIS, pour faire des requêtes SQL il est possible d'utiliser le gestionnaire de Base de Données (*DB manager*)

![Menu > Base de données > DBmanager...](images/menuBDD.png)

Vous pouvez faire des requêtes sur toutes les couches affichées dans le panneau couche (shapefiles, tableaux importés,...) et/ou vous connecter à une base de données (PostGis, SQlite...)
Pour cela, dans le panneau **Fournisseur de données**, cliquer sur la Base de données à requêter ou sur Couches virtuelles → Couches du projet pour requêter des shapefiles.

Pour faire une requête cliquer sur ![l'icône requête SQL](images/iconeSQL.png)

![gestionnaire de BDD - fenêtre requête SQL](images/GBDrequete.png)

> Pour faire une requête avec le gestionnaire de Base de Données il faut:   
>     **1.** choisir la BDD (ou Couches du projet)    
>     **2.** Cliquer sur l'icône ![l'icône requête SQL](images/iconeSQL.png)    
>     **3.** Taper la requête SQL    
>     **4.** Cliquer sur le bouton [Exécuter]

***Note:***

* ![attention](images/attention.png) :construction: ** Définition vue et différence avec Charger une couche ou lien vers une fiche technique vue**
* *Pour créer une vue (définition) cliquer sur le bouton [Créer une vue]*
* *Pour sauvegarder le résultat sous forme de table ou de couche il faut cocher la case* ***Charger en tant que nouvelle couche***

##  Les couches virtuelles

Une autre possibilité est d'utiliser les **couches virtuelles (*virtual layers*)** qui permet de créer une couche virtuelle à partir des couches présentes dans le projets, et d'une requête SQL.

Pour cela il suffit d'ajouter une couche virtuelle  ![l'icône virtual layer](images/iconeVL.png) 

**:construction: a compléter avec imprim ecran**

# Le SQL au quotidien

## Les commentaires

Pour commenter une requête SQL on peut:
* encadrer le commentaire avec `/*` au début et  `*/` à la fin
* en bout de ligne faire préceder le commentaire de 2 tirets ainsi `-- mes commentaires en bout de ligne`

```sql
/* ci-dessous une requête simple */
SELECT * -- sélectionne tous les champs 
FROM tabfait -- depuis la table tabfait */
WHERE "indent" -- commentaire au milieu */ LIKE 'Fossé'
```

> Les commentaires ne seront pas exécutés

## Sélectionner seulement certains champs

```sql
SELECT "indent", "num_fait"
FROM tabfait
LIMIT 4
```

>  Va renvoyer **les 4 premiers enregistrements** de la table en n’affichant que les champs “ident” et “numfait”

*Note: si le "champ" déclaré n'existe pas il sera créé sans données à l'intérieur*

## Les alias

```sql
SELECT  "num_fait" AS numpoly, "indent" AS interpret
FROM tabfait LIMIT 4
```
> Va renvoyer les 4 premiers enregistrements de la table en affichant le champ  “numfait” avec l’intitulé **numpoly ** le champ “ident” avec un nouvel intitulé interpret 

## Rechercher les valeurs uniques

Pour identifier les valeurs uniques on peut utiliser la fonction `distinct`

```sql
SELECT DISTINCT "interpret"
FROM F103066_poly
```

## Rechercher les doublons (Agréger des données)

```sql
SELECT "numpoly", count("numpoly") as nombre_doublons
FROM F103066_poly 
GROUP BY "numpoly" 
HAVING nombre_doublons > 1 
ORDER BY nombre_doublons DESC
```
> Va renvoyer la liste des occurrences du champ “numpoly” et un champ “nombre_doublons” avec le nombre d’occurence.

## Statistiques basiques

Compter le nombre d'enregistrements (lignes) d'une table attributaire:
```sql
SELECT count("StSd") as nb_lignes
 FROM Obernai_ceram_LTD
```
> Va renvoyer le nombre de lignes de la table Obernai_ceram_LTD soit 2141

Faire la somme d'un champ numérique (mais aussi le min, le max et la moyenne):
```sql
SELECT SUM("NR") as total_NR,
 MIN("NR") as min_NR,
 MAX("NR") as max_NR,
 round(AVG("NR"),2) as moy_NR -- la fonction round(valeur, 2) permet d'arrondir à 2 chiffres
 FROM Obernai_ceram_LTD
```
> Va renvoyer la somme des NR (la somme de toutes les valeurs de la colonne "NR") , le minimum, le maximum et la moyenne.

## Faire une jointure

Faire une jointure (relation 1 à 1) entre 2 tables:

Nous allons pour l'exercice:
1) A partir de la table **Obernai_ceram_LTD**, créer un tableau contenant le nombre de NR de céramique par Structure. Charger ce tableau en tant que nouvelle couche sans géométrie  nommée **CeramST** (101 lignes)
```sql
SELECT "St", count("NR") as NR
 FROM Obernai_ceram_LTD
 GROUP BY "St"
```

2) Faire une jointure entre la table **F103066_poly** et **CeramST** sur le champ commun "numpoly" et  "St"
```sql
SELECT *
 FROM F103066_poly as p , CeramST as c
 WHERE p."numpoly" = c."St"
```
:warning: outrageusement long ?! + resultat de 100 lignes + = ou ==

![attention](images/attention.png)**Reprendre la jointure simple avec des données qui si prête du 1 à 1**

![attention](images/attention.png) **ajouter image types de jointures gérées par SQLITE d'après sql.sh**



## Requête d’une relation de 1 à n
```sql
SELECT *
FROM tabfait
JOIN tabus ON tabFait."num_fait" = tabUs."num_fait"
```
> Va renvoyer une table contenant tous les champs de la table fait et de la table us 

*Note:* si les deux tables ont un champ de jointure ayant le même nom on peut utiliser `USING "nom_du_champ"`![attention](images/attention.png) **Verifier si cela fonctionne**

```sql
SELECT *
FROM tabfait
JOIN tabus USING "num_fait" -- si tabfait et tabus ont un champs appellé "num_fait"
```

```sql
SELECT * 
FROM tabfait
JOIN tabus ON tabFait."num_fait" = tabUs."num_fait" 
WHERE tabus."descrip" LIKE '%calage%'
```
>Va renvoyer une table contenant les champs de la table fait pour lesquels le champ description de la table us contient le mot ‘calage’ et le champ “num_fait” un identifiant existant dans la table fait. 

![attention](images/attention.png)**Ajouter une requête avec en plus tabMob et comptage de NR avec group by**

# Le SQL dans l’espace

Comme vous le savez les couches que l’on manipule dans QGIS sont de “simples” **tables attributaires avec une colonne contenant la géométrie**, celle-ci est est  intitulée *geometry* elle peuvent donc être requêtée comme n’importe quelle autre table avec en plus la possibilité de prendre en compte ce champ *geometry*.
Parfois le SQL peut aussi servir à faire des requêtes spatiales on peut dans ce cas utiliser des fonctions de **Spatialite**.

![Attention](images/attention.png) Il est utile de se référer à la liste des [fonctions de Spatialite](http://www.gaia-gis.it/gaia-sins/spatialite-sql-4.3.0.html)

> On entend par requête, comme pour les requêtes SQL non-géométrique d’ailleurs, toute manipulation d’une ou plusieurs tables ayant comme résultat une table.    


```sql
SELECT "num_fait", "ident", "geometry"
FROM tabfait
WHERE "ident" LIKE 'Fossé'
```
> L'appel du champ *"geometry"* dans la clause `SELECT` permet de renvoyer une couche et non pas seulement une table, et par conséquent si on utilise ```*``` le champ "geometry" est pris en compte !
>
> ![Attention](images/attention.png)certains opérateurs SQL géométriques (`ST_centroid`  par ex.) permettent de modifier la géométrie d'une couche dans ce cas le résultat de cette fonction sera suivi de l'alias **geometry** indiquant que c'est le résultat de cette fonction qui servira a définir la géométrie de la couche en sortie. *(Il peut donc y avoir des conflits avec la colonne "geometry" de la couche requêtée si elle a été appelée dans la clause `FROM`)*

![Attention](images/attention.png) Dans les BDD Spatialite et PostGis le champ de géométrie est intitulé **"geom"** et dans les shapefiles il est intitulé **"geometry"**

## Les commentaires "signifiants"
Quand on utilise les **couches virtuelles**, dans une requête SQL certains `/*:commentaires*/ utilisés dans la clause `SELECT`avec ` peuvent être "signifiants".
Par exemple:
  * on peut indiquer le *type* d'un champ en y accolant un commentaire du type `/*:integer*/`, `/*:real*/` ou `/*:string*/`
  * on peut indiquer la *géométrie* et le *SCR* d'une couche en accolant derrière le *geom* un commentaire du type `/*:polygon:2154*/` ou `/*:points:4326*/`

## Interroger caviar 

Le Catalogue de Visualisation de l'Information Archéologique (CAVIAR) est stocké sur un serveur PostGis, on peut donc faire des requêtes SQL pour n'afficher qu'une partie des entités par exemple:

```sql
SELECT p.* 
FROM activite.prescription AS p, activite.communes AS c
WHERE st_intersects(p.geom, c.geom) and c."nom_com" = 'Esvres'
```

> Va renvoyer uniquement les prescriptions de la commune d'Esvres
>
> *Note: pour appeler une table/couche PostGis cela se fait sous la forme: `shema.couche`*
>
> Note: Cette requête est possible sans même ouvrir les couches dans QGIS il suffit de se connecter à Caviar (la BDD PostGis)*

Note: PostGis peut stocker des couches avec des géométries différentes, par exemple dans Caviar la couche PostGis uniteobservation stocke des des polygones et des points

```sql
SELECT *
FROM activite.uniteobservation
WHERE ST_ASTEXT(geom) ILIKE '%POLYGON%'
```
> Va renvoyer une couche contenant les polygones de la couche PostGis uniteobservation.

## Corriger et vérifier les géométries

On peut **vérifier la validité des géométries**  (notamment les nœuds en doubles, les auto-sécants, *ring closure inforcement*)

```sql
SELECT "numpoly", ST_ISVALIDREASON("geometry") AS "invalid_reason", "geometry"
FROM F103066_poly
WHERE ST_ISVALID("geometry") = 0
```

> Va renvoyer une couche contenant les entités ayant des géométries invalides et leur "raison d'invalidité" dans la colonne "invalid_reason" 

Pour **corriger des erreurs de géométries**, on peut utiliser la méthode du buffer 0 (tampon de taille zéro) qui recrée la géométrie mais permet d'éliminer les nœuds dupliqués (*dupicate node*) et les polygones avec auto-contact (*self-contact*).
Attention: cette méthode ne permet pas de corriger les polygones auto-seccants (papillons) à corriger avant, et les polygones de moins de 3 nœuds à corriger après !

```sql
SELECT *, ST_BUFFER(p.geometry, 0) as geometry
FROM F103066_poly as p
```
> Va renvoyer la même couche avec un tampon de 0

## Esvres: Requête 1 à n

```sql
SELECT  "numpoly", F103455_poly."geometry"
FROM F103455_poly
JOIN F103455_point 
	ON "numpoly" = "FAIT"
WHERE "matiere" LIKE 'Verre'
```
> Va renvoyer les Faits (sous forme de polygones) qui contiennent des points dont la "matiere" est du 'Verre'


```sql
SELECT DISTINCT p."numpoly" /*:int*/, p."geometry" /*:polygon:2154*/, count("N_PT") AS nb_objets
	FROM F103455_poly AS p
JOIN F103455_point 
	ON "numpoly" = "FAIT"
WHERE "matiere" LIKE 'Verre'
GROUP BY "FAIT"
ORDER BY nb_objets DESC
```
> Va renvoyer la même chose mais sans doublons de faits et avec le décompte du nombre d’occurrences de la valeur 'verre' par faits. 

## Obernai: cercles proportionnels

Données sources: 
- F103066_ouverture: une couche contenant des sondages.
- Obernai_ceram_LTD.csv: un inventaire de lots de céramique contenant des quantités de céramique dans des sondages 

Requêtes SQL étapes par étapes:
  * Depuis la table Obernai_ceram_LTD, il faut faire une table contenant la somme des NR par StSd 
```sql
SELECT "StSd", SUM("NR") AS somme_nr
FROM Obernai_ceram_LTD 
GROUP BY "StSd"
ORDER BY "StSd"
```
→ sum_nr
  * Depuis la  couche F103066_ouverture, créer une couche de de centroïdes
```sql
SELECT "numouvert", "typouvert", ST_centroid(o.geometry)
FROM F103066_ouverture AS o
```
→ sd_centro

> La fonction `ST_Centroid(geometry)`renvoie le centroïde d'une couche.

  * Faire une jointure des centroïdes d'ouverture (ici ) avec la table contenant la somme des NR
```sql
SELECT sd."numouvert", sd."geometry", nr."somme_nr"
FROM sd_centro AS sd
JOIN sum_nr AS nr
	ON sd."numouvert" = nr."StSd"
```
En une seule requête:
```sql
SELECT o."numouvert", o."typouvert", sum("NR") as somme_nr /*:int*/ , st_centroid(o."geometry") AS geometry /*:point:2154*/
FROM F103066_ouverture AS o
JOIN Obernai_ceram_LTD as c
	ON "numouvert" = "StSd"
GROUP BY "StSd"
```

## Obernai: diagramme origine-destination

Données sources: 

- F103066_poly: une couche contenant les structures.
- Obernai_lithique_LTD.csv: un inventaire par structures (champ "ST") des artefacts lithiques dont des ébauches de meules et des éclats de taille issues de celles-ci. Pour 16 enregistrements la structure meule d'origine est signalée dans le champs "Origine"

Principe:

* relier par une ligne le centroïde des structures contenant des éclats de meules avec celui de la meule dont ils proviennent (indiquée dans le champs "Origine"). 

Requêtes SQL étapes par étapes:

* Depuis la  couche F103066_poly, créer une couche de de centroïdes
```sql
SELECT "numpoly", "typoly", "interpret", "datedebut", "datefin", st_centroid("geometry") AS geometry
    FROM F103066_poly
```
→ poly_centro

* Depuis cette couche de centroïdes poly_centro, joindre la table Obernai_lithique_LTD *uniquement pour les enregistrements contenant une valeur dans le champ "Origine"*:
```sql
SELECT * 
FROM poly_centro
JOIN Obernai_lithique_LTD
	ON "numpoly" = "ST"
WHERE "Origine" IS NOT NULL
```
→ poly_centro_lith

* Tracer le diagramme en oursin reliant le centroïde de la structure de l'éclat à celui de la structure de la meule d'origine.

```sql
SELECT  make_line(p1."geometry", p2."geometry") as geometry,
	   p1."Matiere", p1."Masse" 
FROM poly_centro_lith AS p1
JOIN poly_centro as p2
	ON p2."numpoly" = p1."Origine"
```

→ oursin_meule


> La fonction `make_line(p1.geometry,p2.geometry)`renvoie une couche de polyligne reliant les points de p1 à ceux de p2.
>
> On l'utilise ici:
>
> * p1 = la couche de points *poly_centro_lith* (centroïdes avec inventaire joint, avec une valeur dans le champ "Orgine")
> * p2 = la couche de points *poly_centro* (centroïdes des structures)
> * en faisant une jointure depuis p1 de p2 sur les champs "numpoly" et "Origine"

En une seule requête:

```sql
SELECT make_line(ST_centroid(p1."geometry"),ST_centroid(p2."geometry")) AS geometry,   p1."numpoly",  p1."Origine",  p1."Matiere",  p1."Masse"
FROM (F103066_poly
      JOIN Obernai_lithique_LTD ON "numpoly" = "ST") AS p1
JOIN (F103066_poly
      JOIN Obernai_lithique_LTD	ON "numpoly" = "ST") AS p2
	ON p1."Origine" IS NOT NULL AND p2."numpoly" = p1."Origine"
```

→ oursin_meule

![atention](images/attention.png)Pour limiter les plantages, quand le résultat risque d'être réutilisé il est conseillé de l'enregistrer.

## Esvres: diagramme de remontage

Données sources:

* F103455_point.shp: une couche de points représentant tous les fragments de mobilier archéologique topographiés (dont des tessons de céramiques)  qui contient le numéro d’identifiant "UE"
* S324_3202.csv: une table de jonction  qui contient les liens de remontage entre les tessons d’une même céramique avec une colonne "start" et une colonne "end" qui contiennent les numéros d’identifiants "UE" 

Objectif:

Représenter les liens de remontages à partir d’une table origine-destination et d’une couche de points

```sql
SELECT p1."UE" AS pt_depart,  p2."UE" AS pt_arrivee,
       make_line(p1."geometry", p2."geometry") AS geometry
FROM S324_3202 AS t
     JOIN F103455_point AS p1 ON t."start" = p1."UE"
     JOIN F103455_point AS p2 ON t."end"   = p2."UE"
```

## Esvres: orientation d'un axe

Données sources: 
- F103455_axe: une couche d'axe

Objectif:
Créer un champ contenant l'azimuth (l'angle dans le plan horizontal entre la direction d'un objet et le nord) des axes .

```sql
SELECT "fait" as numpoly, "geometry",
	degrees(st_azimuth(st_startpoint(geometry),st_endpoint(geometry))) as azimuth /*:int*/
FROM F103455_axe
WHERE "typaxe" LIKE 'Lon'
```

Remarque:

* On appelle le champ "geometry" dans la clause `SELECT` pour obtenir une couche de polyligne en sortie
* On ajoute le commentaire significatif `/*:int*/` pour avoir un champ "azimuth" de type entier en sortie
* On a limité les résultats aux axes longitudinaux avec la clause `WHERE`
* On en profite pour renommer le champ "fait" en "numpoly" avec l'utilisation d'un alias

## Esvres: projection de points sur un axe

Données sources: 
- F103455_point: une couche de points représentant des fragments d'objet pris en X,Y,Z
- F103455_axe: une couche d'axe

Objectif:
Projeter les points de la sépulture 24 sur (le plan vertical au droit de) l'axe longitudinal de la sépulture 324.

```sql
SELECT p."fait",p."ue", p."matiere",
make_point(ST_Line_Locate_Point(a."geometry",p."geometry")*st_length(a."geometry"), p.z) AS geometry /*:point:2154*/
FROM F103455_axe AS a, F103455_point AS p 
WHERE a."fait"=324
	AND a."typaxe"='Lon' 
	AND p."fait"=324
```

Détail des fonctions:

* Ceci est la version *One Shot* de la requête SQL qui va renvoyer une couche de points projetés avec les champs indiqués derrière le `SELECT`

* On calcule le x relatif , c'est à dire la distance à l'origine de l'axe de la projection de chaque point (Note: dans la requête en 2 étapes cette valeur est stockée dans la colonne "xrel", ici on a pas besoin de la conserver on s'en sert directement).
  
  Pour ce faire on utilise 2 fonctions SQL géométriques (avec un `ST_`devant):
  
  * `st_line_locate_point (a.geometry, p.geometry)` qui retourne un nombre entre 0 et 1 représentant la distance du point le plus proche (projeté sur la ligne) à l’origine de la ligne sous la forme d’une fraction du total. *par ex, un point au milieu de l'axe renverra  0,5*
  * Comme on obtient une fraction on va multiplier le résultat par la longueur de la ligne pour obtenir une mesure grâce à la fonction `st_length (a.geometry)` qui retourne la longueur de la polyligne.
  
* `make_point(x, y)` permet de tracer des points à partir de coordonnées "x" et "y". Ici on utilise le résultat de `st_line_locate_point()*st_length()`pour x et la donnée du champ "z" pour le y !

{% hint style='tip' %}

On peux aussi fixer une distance maximale par rapport à l'axe des points à prendre en compte en rajoutant à la clause `WHERE` la fonction `PtDistWithin(a."geometry",p."geometry", distance)` (avec la distance exprimée dans l'unité de mesure du SCR, `0.5`pour ne garder que les points à 50 cm à droite et gauche de l'axe).

{% endhint %}



## Alizay: maille

Données sources:
- mobilier_archeo: une couche de points représentant des fragments d'objet.
- grille_10 : une grille de polygone de 10 m de coté.    


```sql
    SELECT g.id, g.geometry as geom, count(*)
    FROM mobilier_archeo as m
    JOIN grille_10 as g ON ST_INTERSECTS(m.geometry, g.geometry)
    GROUP BY g.geometry
```
> Va renvoyer une grille de polygones avec le décompte des mobiliers par carré.

# Ressources

* Pour apprendre le SQL ou  connaître l'utilisation d'une fonction se référer à l'indispensable site  [sql.sh](https://sql.sh) (Fr, SQL en général, non spécifique à un logiciel et sans les opérateurs géométriques).
* Pour les fonctions propres à ![icon spatialite](images/logoSpatialite.png) [spatialite voir le site de référence](http://www.gaia-gis.it/gaia-sins/spatialite-sql-4.2.0.html) (Eng).

* Il existe aussi un cours de @BorisMericskay pour le Master SIGAT de Rennes 2 sous forme de diaporama [![pdf](images/pdf.png)](SeanceQSpatiaLite2.pdf).