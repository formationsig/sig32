# ![sig32](images/sig32.png) SIG 3.2: Analyses Spatiales

* durée : 3 jours
* pré-requis : niveau 1 découverte, niveau 2 Figures, pratique régulière, stat 1 recommandé

![](images/diag_formation_sig32.png)

* objectifs :
  - découvrir et utiliser le SQL pour mettre en œuvre les fonctionnalités du SIG
  - mettre en œuvre des outils d’analyse spatiale (maille, oursins, flux…) appliqués aux données archéologiques

## Supports de formation

* Ce support en ligne: [https://formationsig.gitlab.io/sig32](https://formationsig.gitlab.io/sig32) (existe aussi en version PDF, cf. lien dédié en haut de page)

* Le Diaporama en ligne sur (https://slides.com/archeomatic/sig32) ajouter [/live](https://slides.com/archeomatic/perf/live) pour la diffusion synchronisée et en direct lors de la formation. 

  > Se déplacer dans le diaporama  avec les touches ↑↓→← du clavier et la touche [Echap.] pour voir l'ensemble.


Des documents récapitulatifs et d'aide (il est conseillé d'en avoir une version imprimée durant la formation):

* Le [mémo SQL](https://formationsig.gitlab.io/sig32/memoSQL/memoSQL.html) (document commun avec la formation [SIG 3.1 BDD Relationelle & Spatiale](https://formationsig.gitlab.io/sig31))
*  Le Récapitulatif des [Tables Attributaires des données d'exercice](https://formationsig.gitlab.io/sig31/pas_a_pas/SIG32_Tables_Attributaires.html) , aussi en version PDF  [![pdf](images/pdf.png)](https://gitlab.com/formationsig/sig31/-/raw/master/pas_a_pas/SIG32_Tables_Attributaires.pdf?inline=false) 

*Note: les données exercices sont en [accès restreint pour les formateurs](\\promethese\partages-siege\Partages_Nationaux\Formations SIG\) et fournies aux stagiaires lors de la formation en présentiel.*

## Ressources et documentation

* [Fiches Techniques](https://formationsig.gitlab.io/fiches-techniques)
* [Guide pratique CAVIAR](https://formationsig.gitlab.io/caviar/)
* [Guide pratique des 6 couches (pdf)](https://docs.google.com/a/inrap.fr/viewer?a=v&pid=sites&srcid=aW5yYXAuZnJ8cmVzZWF1LXJlZmVyZW50cy1zaWd8Z3g6MTY2NTliYzRjNzc1Yjk4Yg)
* Note DST - organisation du NAS (à faire)
* [Raccourcis clavier QGIS (pdf)](https://docs.google.com/a/inrap.fr/viewer?a=v&pid=sites&srcid=aW5yYXAuZnJ8cmVzZWF1LXJlZmVyZW50cy1zaWd8Z3g6OTIyMDEyNTlkMTVlYzBl)

* Le dépôt Gitlab à de ce support de formation mis à jour sur (https://gitlab.com/formationsig/sig32) 



[ :leftwards_arrow_with_hook: Accueil - Formation SIG](https://formationsig.gitlab.io/toc/)



Toute cette documentation est sous licence sous licence [![CC](images/CC_BY_ND.png)](https://creativecommons.org/licenses/by-nc-sa/3.0/fr/)